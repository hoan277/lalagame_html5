﻿function update_score(slug, user, level, point){
    if (!navigator.onLine) { return; }
    let objScore = {gameSlug: slug, userId: user, gameLevel: level, gameScore: point}
    $.ajax({
        url:"http://183.81.35.24:7001/Score/Update",
        method:"POST",
        data:{
            score: objScore
        },
        success: function(data){
            console.log(data);
        },
        error: function(xhr){
            console.log("Lỗi: " + xhr.resposeText);
        },
    });
}
let game;
let gameOptions = {
    gemSize: 100,
    swapSpeed: 200,
    fallSpeed: 100,
    destroySpeed: 200,
    boardOffset: {
        x: 100,
        y: 250
    }
}
// Khai báo biến ảnh, âm thanh
var score = 0;
var scoreText, levelText, huychuong_text, end;
var right, right1, right2, combo, wrong, botbien, tangcap;
var frame_huongdan;
var current_level = localStorage.getItem("level") || 1;
var plus;
var level = {
    1: "babyshark",
    2: "fathershark",
    3: "mothershark",
    4: "thochi",
    5: "thoem",
    6: "voicam",
    7: "voihong",
    8: "voitim",
}
var level_object = {
    babyshark: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0],
        2: [name = "khung", x = 380, y = 580, xx = 380, yy = 580],
        3: [name = "done", x = 380, y = 580, xx = -380, yy = -280],
        4: [name = "dau", x = 487, y = 552, xx = 165, yy = 740],
        5: [name = "duoi", x = 230, y = 509, xx = 420, yy = 320],
        6: [name = "mieng", x = 508, y = 613, xx = 680, yy = 370],
        7: [name = "thantren", x = 349, y = 540, xx = 635, yy = 754],
        8: [name = "thanduoi", x = 369, y = 614, xx = 135, yy = 342],
    },
    fathershark: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0],
        2: [name = "khung", x = 380, y = 580, xx = 380, yy = 580],
        3: [name = "done", x = 380, y = 580, xx = -380, yy = -280],
        4: [name = "dau", x = 569, y = 547, xx = 139, yy = 831],
        5: [name = "duoi", x = 129, y = 504, xx = 705, yy = 735],
        6: [name = "mieng", x = 574, y = 639, xx = 634, yy = 326],
        7: [name = "thantren", x = 324, y = 528, xx = 456, yy = 995],
        8: [name = "thanduoi", x = 330, y = 629, xx = 188, yy = 324],
    },
    mothershark: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0],
        2: [name = "khung", x = 380, y = 580, xx = 380, yy = 580],
        3: [name = "done", x = 380, y = 580, xx = -380, yy = -280],
        4: [name = "dau", x = 577, y = 568, xx = 132, yy = 855],
        5: [name = "duoi", x = 134, y = 481, xx = 690, yy = 777],
        6: [name = "mieng", x = 546, y = 649, xx = 649, yy = 328],
        7: [name = "thantren", x = 317, y = 557, xx = 454, yy = 1004],
        8: [name = "thanduoi", x = 335, y = 658, xx = 235, yy = 329],
    },
    thochi: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0],
        2: [name = "khung", x = 370, y = 763, xx = 370, yy = 763],
        3: [name = "done", x = 370, y = 763, xx = -200, yy = -200],
        4: [name = "dau", x = 375, y = 765, xx = 565, yy = 279],
        5: [name = "chan", x = 355, y = 1066, xx = 159, yy = 294],
        6: [name = "tay", x = 322, y = 939, xx = 635, yy = 752],
        7: [name = "tai", x = 377, y = 533, xx = 559, yy = 1095],
        8: [name = "carot", x = 496, y = 953, xx = 89, yy = 681],
    },
    thoem: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0],
        2: [name = "khung", x = 390, y = 763, xx = 390, yy = 763],
        3: [name = "done", x = 370, y = 763, xx = -300, yy = -200],
        4: [name = "dau", x = 320, y = 773, xx = 615, yy = 313],
        5: [name = "chan", x = 357, y = 1062, xx = 90, yy = 940],
        6: [name = "tay", x = 356, y = 968, xx = 690, yy = 680],
        7: [name = "tai", x = 390, y = 529, xx = 174, yy = 308],
        8: [name = "carot", x = 533, y = 1050, xx = 110, yy = 615],
    },
    voicam: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0],
        2: [name = "khung", x = 366, y = 800, xx = 350, yy = 800],
        3: [name = "done", x = 365, y = 900, xx = -300, yy = -200],
        4: [name = "dau", x = 370, y = 753, xx = 604, yy = 320],
        5: [name = "chantruoc", x = 395, y = 963, xx = 147, yy = 400],
        6: [name = "chansau", x = 555, y = 944, xx = 165, yy = 1105],
        7: [name = "voi", x = 200, y = 894, xx = 651, yy = 1145],
        8: [name = "toc", x = 264, y = 614, xx = 718, yy = 701],
    },
    voihong: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0],
        2: [name = "khung", x = 366, y = 900, xx = 350, yy = 920],
        3: [name = "done", x = 365, y = 900, xx = -300, yy = -200],
        4: [name = "dau", x = 371, y = 870, xx = 535, yy = 395],
        5: [name = "chantruoc", x = 416, y = 1043, xx = 700, yy = 666],
        6: [name = "chansau", x = 544, y = 1037, xx = 120, yy = 590],
        7: [name = "voi", x = 147, y = 920, xx = 160, yy = 330],
        8: [name = "toc", x = 360, y = 766, xx = 678, yy = 836],
    },
    voitim: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0],
        2: [name = "khung", x = 366, y = 900, xx = 350, yy = 770],
        3: [name = "done", x = 365, y = 900, xx = -350, yy = -200],
        4: [name = "dau", x = 383, y = 764, xx = 234, yy = 294],
        5: [name = "chantruoc", x = 408, y = 974, xx = 89, yy = 620],
        6: [name = "chansau", x = 554, y = 961, xx = 660, yy = 381],
        7: [name = "voi", x = 188, y = 862, xx = 637, yy = 1167],
        8: [name = "toc", x = 352, y = 600, xx = 165, yy = 1092],
    }
}
window.onload = function () {
    let gameConfig = {
        width: 800,
        height: 1300,
        scene: playGame,
        backgroundColor: 0x222222
    }
    game = new Phaser.Game(gameConfig);
    window.focus();
    resize();
    window.addEventListener("resize", resize, false);
}
class playGame extends Phaser.Scene {
    constructor() {
        super("PlayGame");
    }
    preload() {
         // Setup mp3
         let lstMp3 = ["right", "right1", "right2", "combo", "wrong", "botbien"];
         lstMp3.forEach(e => this.load.audio(e, '../mp3/' + e + '.mp3', {instances: 1}));
        // this.load.image('huongdan', 'images/huongdan.png');
        this.load.image('background', 'images/' + level[current_level] + '/bg.png');
        this.load.image('end', '../images/end.png');
        this.load.image('home', '../images/home.png');
        this.load.image('tangcap', '../images/tangcap.png');
        this.load.image('back', '../images/back.png');
        this.load.image('huychuong', '../images/huychuong.png');
        this.load.image('score', '../images/score.png');
        this.load.image('khung', 'images/' + level[current_level] + '/khung.png');
        this.load.image('bg', 'images/' + level[current_level] + '/bg.png');
        this.load.image('dau', 'images/' + level[current_level] + '/dau.png');
        this.load.image('done', 'images/' + level[current_level] + '/done.png');
        if (current_level <= 3) {
            this.load.image('duoi', 'images/' + level[current_level] + '/duoi.png');
            this.load.image('mieng', 'images/' + level[current_level] + '/mieng.png');
            this.load.image('thanduoi', 'images/' + level[current_level] + '/thanduoi.png');
            this.load.image('thantren', 'images/' + level[current_level] + '/thantren.png');
        }
        if (current_level == 4 || current_level == 5) {
            this.load.image('tay', 'images/' + level[current_level] + '/tay.png');
            this.load.image('chan', 'images/' + level[current_level] + '/chan.png');
            this.load.image('tai', 'images/' + level[current_level] + '/tai.png');
            this.load.image('carot', 'images/' + level[current_level] + '/carot.png');
        }
        if (current_level > 5) {
            this.load.image('voi', 'images/' + level[current_level] + '/voi.png');
            this.load.image('toc', 'images/' + level[current_level] + '/toc.png');
            this.load.image('chantruoc', 'images/' + level[current_level] + '/chantruoc.png');
            this.load.image('chansau', 'images/' + level[current_level] + '/chansau.png');
        }
        scoreText = this.add.text(25, 150, "Điểm: 0", {
            fontSize: "32px",
            fill: "#FF030F",
            fontFamily: "Pony"
        });
        levelText = this.add.text(320, 145, "Cấp độ " + current_level, {
            fontSize: "40px",
            fill: "#FDD691",
            fontFamily: "Pony"
        });
        plus = this.add.text(590, 60, "01", {
            fontSize: "60px",
            fill: "#F5BC2E",
            fontFamily: "Pony"
        });
    }
    update(){
        if(tangcap.visible){
            if (tangcap.y > -50){
                console.log(tangcap.y--);
                tangcap.y = tangcap.y--;
                if (current_level >= 7) {
                    update_score("puzzle", localStorage.getItem("userId"), current_level - 1, score);
                }
                location.reload();
                tangcap.visible = false;
            }
        }
    }
    create() {
        this.load_background();
        this.load_default_button();
        this.load_image();
        this.load_mp3();
        this.canPick = true;
        this.dragging = true;
    }
    form_load() {
        // if (frame_huongdan.visible) {
        //     frame_huongdan.visible = false;
        //     localStorage.setItem("frame_huongdan", false);
        // }
    }
    huychuong_btn_click() {
        if(current_level < 8){
            current_level++;
            localStorage.setItem("level",current_level++);
            location.reload();
        }
    }
    back_click() {
        if (current_level > 1) {
            current_level--;
            localStorage.setItem("level", current_level);
            location.reload();
        }
    }
    home_click() {
        location.href = "../index.html";
        // frame_huongdan.visible = true;
        // plus.visible = false;
        // frame_huongdan.setDepth(1);
    }
    load_background() {
        // Cài đặt background
        let form_background = this.add.image(this.cameras.main.width / 2, this.cameras.main.height / 2, 'background');
        let scaleX = this.cameras.main.width / form_background.width;
        let scaleY = this.cameras.main.height / form_background.height;
        let scale = Math.max(scaleX, scaleY);
        form_background.setScale(scale).setScrollFactor(0);
        form_background.setInteractive({
            useHandCursor: false
        }).on("pointerdown", this.form_load);
    }
    load_mp3() {
        
        // Tải nhạc
        right = this.sound.add('right');
        right1 = this.sound.add('right1');
        right2 = this.sound.add('right2');
        combo = this.sound.add('combo');
        wrong = this.sound.add('wrong');
        botbien = this.sound.add('botbien');
        // botbien.setLoop(true);
        // botbien.play();
    }
    load_image() {
        // frame_huongdan = this.add.image(400,500, "huongdan");
        // frame_huongdan.displayWidth = 800;
        // frame_huongdan.displayHeight = 900;
        var correct_position = new Array();
        var check_point = 0;
        var right_complete = new Array();
        for (var i = 1; i <= 8; i++) {
            let obj_level = level_object[level[current_level]]; // babyshark, fathershark,.....
            let prop = obj_level[i];
            let name = prop[0];
            let x = prop[1];
            let y = prop[2];
            let xx = prop[3];
            let yy = prop[4];
            if (name == "bg")
                continue;
            let sprites = this.add.image(xx, yy, name);
            sprites.displayWidth = sprites.width / 2;
            sprites.displayHeight = sprites.height / 2;
            if (name == "khung"){
                sprites.setScale(0.6);
                continue;
            }
            //  Tạo action kéo thả
            sprites.setDepth(1);
            sprites.setScale(0.6);
            sprites.setInteractive({
                useHandCursor: true
            });
            correct_position[sprites.texture.key] = new Array(x, y);
            this.input.setDraggable(sprites);
            this.input.on('dragstart', function (pointer, gameObject) {
                this.children.bringToTop(gameObject);
            }, this);
            this.input.on('drag', function (pointe, gameObject, dragX, dragY) {
                gameObject.x = dragX;
                gameObject.y = dragY;
                gameObject.setOrigin(0.5);
                for (let j = 1; j <= 8; j++) {
                    let x = correct_position[gameObject.texture.key][0];
                    let y = correct_position[gameObject.texture.key][1];
                    console.log(Math.floor(dragX) + " - " + Math.floor(dragY));
                    console.log(x + " - " + y)
                    if ((Math.floor(dragX) == x && Math.floor(dragY) == y)) {
                        right_complete[gameObject.texture.key] = new Array("ok");
                        gameObject.x = x;
                        gameObject.y = y;
                        console.log("Chính xác 100%");
                        // if(!right2.play()){
                        //     right2.play();
                        // }
                        
                    } else if (
                        ((x - Math.floor(dragX) + 15) > 5 && (x - Math.floor(dragX) - 15) < 7) &&
                        ((y - Math.floor(dragY) + 15) > 5 && (y - Math.floor(dragY) - 15) < 7)
                    ) {
                        right_complete[gameObject.texture.key] = new Array("ok");
                        gameObject.x = x;
                        gameObject.y = y;
                        console.log("Đúng tọa độ");
                        // if(!right2.play()){
                        //     right2.play();
                        // }
                        // return;
                    }
                }
                if (Object.keys(right_complete).length == 5) {
                    if (check_point < 1) {
                        check_point = 1;
                        if(current_level == 8){
                            end.visible = true;
                            end.setDepth(99999);
                            return;
                        }
                        if (check_point == 1) {
                            if (current_level == 8){
                                current_level = 0;
                            }
                            current_level++;
                            localStorage.setItem("level", current_level);
                            combo.play()
                            setTimeout(function(){
                                tangcap.visible = true;
                            },2000);
                            // location.reload();
                        }
                    }
                }
            });
        }
    }
    load_default_button() {
        // Cài đặt nút home
        let home_btn = this.add.image(70, 70, "home");
        home_btn.displayWidth = 90;
        home_btn.displayHeight = 90;
        // Cài đặt nút back
        let back_btn = this.add.image(180, 70, "back");
        back_btn.displayWidth = 120;
        back_btn.displayHeight = 90;
        // Cài đặt hình nền của khung đếm điểm
        let score_btn = this.add.image(400, 150, "score");
        score_btn.displayWidth = 400;
        score_btn.displayHeight = 250;
        // Cài đặt huy chương
        let huychuong_btn = this.add.image(720, 90, "huychuong");
        huychuong_btn.displayWidth = 140;
        huychuong_btn.displayHeight = 150;
         // Cài đặt nút tăng cấp
        tangcap = this.add.image(400,100, "tangcap");
        tangcap.displayWidth = 300;
        tangcap.displayHeight = 120;
        tangcap.visible = false;
        tangcap.setDepth(1);
        scoreText = this.add.text(25, 150, "Điểm: 0", {
            fontSize: "32px",
            fill: "#FF030F",
            fontFamily: "Pony"
        });
        levelText = this.add.text(320, 145, "Cấp độ " + current_level, {
            fontSize: "40px",
            fill: "#FDD691",
            fontFamily: "Pony"
        });
        plus = this.add.text(580, 33, "+", {
            fontSize: "60px",
            fill: "#F5BC2E",
            fontFamily: "Pony"
        });
        huychuong_text = this.add.text(590, 60, "01", {
            fontSize: "60px",
            fill: "#F5BC2E",
            fontFamily: "Pony"
        });
        plus.visible = false;
        home_btn.setInteractive({
            useHandCursor: true
        }).on('pointerdown', this.home_click);
        huychuong_btn.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.huychuong_btn_click);
        back_btn.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.back_click);
        // Cài đặt nút END
        end = this.add.image(450, 600, "end");
        end.setScale(0.4);
        end.setDepth(999999);
        end.visible = false;
        end.setInteractive( { useHandCursor: true  }).on("pointerdown", this.home_click);
    }
}
function resize() {
    var canvas = document.querySelector("canvas");
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var windowRatio = windowWidth / windowHeight;
    var gameRatio = game.config.width / game.config.height;
    if (windowRatio < gameRatio) {
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    } else {
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}