﻿function update_score(slug, user, level, point){
    if (!navigator.onLine) { return; }
    let objScore = {gameSlug: slug, userId: user, gameLevel: level, gameScore: point}
    $.ajax({
        url:"http://183.81.35.24:7001/Score/Update",
        method:"POST",
        data:{
            score: objScore
        },
        success: function(data){
            console.log(data);
        },
        error: function(xhr){
            console.log("Lỗi: " + xhr.resposeText);
        },
    });
}
//update_score("puzzle", localStorage.getItem("userId"), current_level, score);
let game;
let gameOptions = {
    gemSize: 100,
    swapSpeed: 200,
    fallSpeed: 100,
    destroySpeed: 200,
    boardOffset: {
        x: 100,
        y: 250
    }
}
// Khai báo biến ảnh, âm thanh
var score = 0;
var current_color = "";
var colors_selected = [];
var scoreText, levelText, huychuong_text;
var right, right1, right2, combo, wrong, botbien, tangcap, end;
var frame_huongdan;
var current_level = localStorage.getItem("level") || 1;
var plus;
var datomau = false;
var check_color = {};
var tint_color = "";
var tint_color_str = "";
var level = {
    1: "grandmashark",
    2: "grandpashark",
    3: "thoem",
    4: "unin1",
    5: "unin2",
    6: "unin3",
}

var hongsao, hongcaro;
function get_current_color(color){
    switch(color.toString()){
        case "xanhnuocbien":
            tint_color = 0x45FFFD;
            tint_color_str = "0x45FFFD";
            break;
        case "dacamnhat":
            tint_color = 0xF57818;
            tint_color_str = "0xF57818";
            break;
        case "xanhlacaynhat":
            tint_color = 0xD7FDE3;
            tint_color_str = "0xD7FDE3";
            break;
        case "xanhlacaydam":
            tint_color = 0x6FCE70;
            tint_color_str = "0x6FCE70";
            break;
        case "xanhlacay":
            tint_color = 0x0C722D;
            tint_color_str = "0x0C722D";
            break;
        case "xanhtim":
            tint_color = 0xA21EC2; //14115772
            tint_color_str = "0xA21EC2";
            break;
        case "dacamdam":
            tint_color = 0xD95116;
            tint_color_str = "0xD95116";
            break;
        case "hongcaro":
            tint_color = 0x933537;
            tint_color_str = "0x933537";
            break;
        case "hongdam":
            tint_color = 0xE69790;//12416134
            tint_color_str = "0xE69790";
            break;
        case "hongnhat":
            tint_color = 0xFEBCB8;
            tint_color_str = "0xFEBCB8";
            break;
        case "hongsao":
            break;
        case "nau":
            tint_color = 0x914304;
            tint_color_str = "0x914304";
            break;
        case "naudam":
            tint_color = 0x9E402D;
            tint_color_str = "0x9E402D";
            break;
        case "naunhat":
            tint_color = 0xFEE7D5;
            tint_color_str = "0xFEE7D5";
            break;
        case "vang":
            tint_color = 0xFFF045;
            tint_color_str = "0xFFF045";
            break;
        case "vangdam":
            tint_color = 0xEBC22D;
            tint_color_str = "0xEBC22D";
            break;
        case "vangnghe":
            tint_color = 0xF49501;
            tint_color_str = "0xF49501";
            break;
        default:
            tint_color = 0xFFFFFF;
            tint_color_str = "0xFFFFFF";
            break;
    }
}
var level_object = {
    grandmashark: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0, color = "0xffffff"],
        2: [name = "khung", x = 380, y = 580, xx = 380, yy = 580, color = "0xffffff"],
        3: [name = "done", x = 380, y = 580, xx = -380, yy = -280, color = "0xffffff"],
        4: [name = "dau", x = 533, y = 529, xx = 165, yy = 740, color = "0x0C722D"],
        5: [name = "duoi", x = 174, y = 573, xx = 351, yy = 864, color = "0x0C722D"],
        6: [name = "mieng", x = 545, y = 615, xx = 244, yy = 346, color = "0xD7FDE3"],
        7: [name = "thantren", x = 340, y = 555, xx = 635, yy = 754, color = "0x0C722D"],
        8: [name = "thanduoi", x = 340, y = 651, xx = 640, yy = 370, color = "0xD7FDE3"],
        9: [name = "hinhmau", x = 100, y = 1000, xx = 100, yy = 1000, color = "0xffffff"],
    },
    grandpashark: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0, color = "0xffffff"],
        2: [name = "khung", x = 380, y = 580, xx = 380, yy = 580, color = "0xffffff"],
        3: [name = "done", x = 380, y = 580, xx = -380, yy = -280, color = "0xffffff"],
        4: [name = "dau", x = 531, y = 556, xx = 165, yy = 740, color = "0x914304"],
        5: [name = "duoi", x = 180, y = 505, xx = 420, yy = 320, color = "0x914304"],
        6: [name = "mieng", x = 515, y = 642, xx = 660, yy = 370, color = "0xFEE7D5"],
        7: [name = "thantren", x = 341, y = 552, xx = 635, yy = 754, color = "0x914304"],
        8: [name = "thanduoi", x = 326, y = 620, xx = 135, yy = 342, color = "0xFEE7D5"],
        9: [name = "hinhmau", x = 100, y = 1000, xx = 100, yy = 1000, color = "0xffffff"],
    },
    // fathershark: {
    //     1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0, color = "0xffffff"],
    //     2: [name = "khung", x = 380, y = 580, xx = 380, yy = 580, color = "0xffffff"],
    //     3: [name = "done", x = 380, y = 580, xx = -380, yy = -280, color = "0xffffff"],
    //     4: [name = "dau", x = 569, y = 547, xx = 139, yy = 831, color = "0xffffff"],
    //     5: [name = "duoi", x = 129, y = 504, xx = 705, yy = 735, color = "0xffffff"],
    //     6: [name = "mieng", x = 574, y = 639, xx = 634, yy = 326, color = "0xffffff"],
    //     7: [name = "thantren", x = 324, y = 528, xx = 456, yy = 995, color = "0xffffff"],
    //     8: [name = "thanduoi", x = 330, y = 629, xx = 188, yy = 324, color = "0xffffff"],
    //     9: [name = "hinhmau", x = 100, y = 1000, xx = 100, yy = 1000, color = "0xffffff"],
    // },
    // mothershark: {
    //     1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0, color = "0xffffff"],
    //     2: [name = "khung", x = 380, y = 580, xx = 380, yy = 580, color = "0xffffff"],
    //     3: [name = "done", x = 380, y = 580, xx = -380, yy = -280, color = "0xffffff"],
    //     4: [name = "dau", x = 577, y = 568, xx = 132, yy = 855, color = "0xffffff"],
    //     5: [name = "duoi", x = 134, y = 481, xx = 690, yy = 777, color = "0xffffff"],
    //     6: [name = "mieng", x = 546, y = 649, xx = 649, yy = 328, color = "0xffffff"],
    //     7: [name = "thantren", x = 317, y = 557, xx = 454, yy = 1004, color = "0xffffff"],
    //     8: [name = "thanduoi", x = 335, y = 658, xx = 235, yy = 329],
    //     9: [name = "hinhmau", x = 100, y = 1000, xx = 100, yy = 1000, color = "0xffffff"],
    // },
    thoem: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0, color = "0xffffff"],
        2: [name = "khung", x = 450, y = 730, xx = 450, yy = 730, color = "0xffffff"],
        3: [name = "done", x = 370, y = 763, xx = -200, yy = -200, color = "0xffffff"],
        4: [name = "dau", x = 403, y = 751, xx = 639, yy = 378, color = "0xF49501"],
        5: [name = "chan", x = 432, y = 953, xx = 177, yy = 483, color = "0xF49501"],
        6: [name = "tay", x = 423, y = 874, xx = 678, yy = 704, color = "0xD95116"],
        7: [name = "tai", x = 460, y = 571, xx = 362, yy = 292, color = "0xF49501"],
        8: [name = "carot", x = 562, y = 944, xx = 171, yy = 736, color = "0xF49501"],
        9: [name = "hinhmau", x = 100, y = 1000, xx = 100, yy = 1000, color = "0xffffff"],
    },
    unin1: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0, color = "0xffffff"],
        2: [name = "khung", x = 420, y = 730, xx = 420, yy = 730, color = "0xffffff"],
        3: [name = "done", x = 370, y = 763, xx = -300, yy = -200, color = "0xffffff"],
        4: [name = "dau", x = 445, y = 600, xx = 391, yy = 1099, color = "0xFEBCB8"],
        5: [name = "chan", x = 430, y = 925, xx = 677, yy = 871, color = "0xFEBCB8"],
        6: [name = "tay", x = 421, y = 754, xx = 274, yy = 358, color = "0xFEBCB8"],
        7: [name = "than", x = 439, y = 811, xx = 112, yy = 595, color = "0xFFF045"],
        8: [name = "hinhmau", x = 100, y = 1000, xx = 100, yy = 1000, color = "0xffffff"],
        9: [name = "mieng", x = 420, y = 609, xx = 614, yy = 329, color = "0xFEBCB8"],
    },
    unin2: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0, color = "0xffffff"],
        2: [name = "khung", x = 366, y = 800, xx = 350, yy = 800, color = "0xffffff"],
        3: [name = "done", x = 365, y = 900, xx = -300, yy = -200, color = "0xffffff"],
        4: [name = "dau", x = 350, y = 798, xx = 604, yy = 320, color = "0xffffff"],
        5: [name = "chan", x = 347, y = 935, xx = 147, yy = 400, color = "0xffffff"],
        6: [name = "tay", x = 348, y = 806, xx = 137, yy = 635, color = "0xffffff"],
        7: [name = "than", x = 350, y = 814, xx = 633, yy = 1050, color = "0xffffff"],
        8: [name = "mieng", x = 378, y = 676, xx = 680, yy = 700, color = "0xffffff"],
        9: [name = "hinhmau", x = 100, y = 1000, xx = 100, yy = 1000, color = "0xffffff"],
        10:[name = "muhongsao", x = 100, y = 1000, xx = 110, yy = 1000, color = "0xffffff"],
        11:[name = "aohongcaro", x = 100, y = 1000, xx = 110, yy = 1000, color = "0xffffff"],
    },
    unin3: {
        1: [name = "bg", x = 0, y = 0, xx = 0, yy = 0, color = "0xffffff"],
        2: [name = "khung", x = 366, y = 900, xx = 350, yy = 920, color = "0xffffff"],
        3: [name = "done", x = 365, y = 900, xx = -300, yy = -200, color = "0xffffff"],
        4: [name = "dau", x = 371, y = 868, xx = 535, yy = 395, color = "0xffffff"],
        5: [name = "chantruoc", x = 415, y = 1044, xx = 700, yy = 666, color = "0xffffff"],
        6: [name = "chansau", x = 544, y = 1037, xx = 120, yy = 590, color = "0xffffff"],
        7: [name = "voi", x = 147, y = 920, xx = 160, yy = 330, color = "0xffffff"],
        8: [name = "toc", x = 360, y = 766, xx = 678, yy = 836, color = "0xffffff"],
        9: [name = "hinhmau", x = 100, y = 1000, xx = 100, yy = 1000, color = "0xffffff"],
    },
}
window.onload = function () {
    let gameConfig = {
        width: 800,
        height: 1420,
        scene: playGame,
        backgroundColor: 0x222222
    }
    game = new Phaser.Game(gameConfig);
    window.focus();
    resize();
    window.addEventListener("resize", resize, false);
}
class playGame extends Phaser.Scene {
    constructor() {
        super("PlayGame");
    }
    preload() {
        // Setup mp3
        let lstMp3 = ["right", "right1", "right2", "combo", "wrong", "botbien"];
        lstMp3.forEach(e => this.load.audio(e, '../mp3/' + e + '.mp3', {
            instances: 1
        }));
        // Setup Image
        let lstImage = ["tangcap", "choilai", "hoanthanh", "back", "huychuong", "home", "help", "score", "bangmau"];
        lstImage.forEach(e => this.load.image(e, '../images/' + e + '.png'));
        // Setup Bảng màu
        let lstbangmau = ["brush2","dacamdam", "dacamnhat", "hongcaro", "hongdam", "hongnhat", "hongsao", "nau", "nau", "naudam", "naunhat", "vang", "vangdam", "vangnghe", "xanhlacay", "xanhlacaydam", "xanhlacaynhat", "xanhnuocbien", "xanhtim", ];
        lstbangmau.forEach(e => this.load.image(e, 'images/bangmau/' + e + '.png'));
        this.load.image('huongdan', 'images/huongdan.png');
        this.load.image("end", '../images/end.png');
        this.load.image('background', 'images/' + level[current_level] + '/bg.png');
        this.load.image('khung', 'images/' + level[current_level] + '/khung.png');
        this.load.image('bg', 'images/' + level[current_level] + '/bg.png');
        this.load.image('dau', 'images/' + level[current_level] + '/dau.png');
        this.load.image('done', 'images/' + level[current_level] + '/done.png');
        this.load.image('hinhmau', 'images/' + level[current_level] + '/hinhmau.png');
        if (current_level <= 2) {
            this.load.image('duoi', 'images/' + level[current_level] + '/duoi.png');
            this.load.image('mieng', 'images/' + level[current_level] + '/mieng.png');
            this.load.image('thanduoi', 'images/' + level[current_level] + '/thanduoi.png');
            this.load.image('thantren', 'images/' + level[current_level] + '/thantren.png');
        }
        if (current_level == 3) {
            this.load.image('tay', 'images/' + level[current_level] + '/tay.png');
            this.load.image('chan', 'images/' + level[current_level] + '/chan.png');
            this.load.image('tai', 'images/' + level[current_level] + '/tai.png');
            this.load.image('carot', 'images/' + level[current_level] + '/carot.png');
        }
        if (current_level == 4) {
            this.load.image('than', 'images/' + level[current_level] + '/than.png');
            this.load.image('tay', 'images/' + level[current_level] + '/tay.png');
            this.load.image('mieng', 'images/' + level[current_level] + '/mieng.png');
            this.load.image('chan', 'images/' + level[current_level] + '/chan.png');
            this.load.image('mat', 'images/' + level[current_level] + '/mat.png');
        }
        if (current_level == 5) {
            this.load.image('than', 'images/' + level[current_level] + '/than.png');
            this.load.image('tay', 'images/' + level[current_level] + '/tay.png');
            this.load.image('mieng', 'images/' + level[current_level] + '/mieng.png');
            this.load.image('chan', 'images/' + level[current_level] + '/chan.png');
            this.load.image('mat', 'images/' + level[current_level] + '/mat.png');
            this.load.image('muhongsao', 'images/' + level[current_level] + '/muhongsao.png');
            this.load.image('aohongcaro', 'images/' + level[current_level] + '/aohongcaro.png');
        }
        scoreText = this.add.text(25, 150, "Điểm: 0", {
            fontSize: "32px",
            fill: "#FF030F",
            fontFamily: "Pony"
        });
        levelText = this.add.text(320, 145, "Cấp độ " + current_level, {
            fontSize: "40px",
            fill: "#FDD691",
            fontFamily: "Pony"
        });
        plus = this.add.text(590, 60, "01", {
            fontSize: "60px",
            fill: "#F5BC2E",
            fontFamily: "Pony"
        });
    }
    update() {
        if (tangcap.visible) {
            if (tangcap.y > -50) {
                console.log(tangcap.y--);
                tangcap.y = tangcap.y--;
                if (current_level >= 5) {
                    update_score("tomau", localStorage.getItem("userId"), current_level, score);
                }
                location.reload();
            }
        }
    }
    play_again_click() {
        localStorage.clear();
        location.reload();
    }
    create() {


        
        this.load_background();
        this.load_default_button();
        this.load_image();
        this.load_mp3();
        this.canPick = true;
        this.dragging = true;
        // Cài đặt bảng màu
        let bangmau = this.add.image(400, 1300, "bangmau");
        bangmau.displayWidth = 800;
        bangmau.displayHeight = 350;
        // Cài đặt nút chơi lại
        let play_again = this.add.image(630, 1280, "choilai");
        play_again.setScale(0.5);
        play_again.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.play_again_click);
        // Cài đặt nút hoàn thành
        let hoanthanh = this.add.image(640, 1350, "hoanthanh");
        hoanthanh.setScale(0.5);
        let count_color = 0;
        hoanthanh.setInteractive({
            useHandCursor: true
        }).on("pointerdown", function () {
            if (check_color != null) {
                count_color = 0;
                let obj_level = level_object[level[current_level]]; // babyshark, fathershark,.....
                let count_props = Object.keys(obj_level).length;
                console.log(check_color);
                for (let index = 1; index <= count_props; index++) {
                    let bophan = obj_level[index];
                    let count_prop_bophan = Object.keys(bophan).length;
                    let ten_bophan = bophan[0];
                    let color = bophan[5];
                    // console.log(color);
                    // console.log(ten_bophan);
                    if (check_color.hasOwnProperty("mieng") && ten_bophan == "mieng") {
                        if (check_color.mieng == color) {
                            count_color += 1;
                        }
                    }
                    if (check_color.hasOwnProperty("dau") && ten_bophan == "dau") {
                        if (check_color.dau == color) {
                            count_color += 1;
                        }
                    }
                    if (check_color.hasOwnProperty("duoi") && ten_bophan == "duoi") {
                        if (check_color.duoi == color) {
                            count_color += 1;
                        }
                    }
                    if (check_color.hasOwnProperty("thantren") && ten_bophan == "thantren") {
                        if (check_color.thantren == color) {
                            count_color += 1;
                        }
                    }
                    if (check_color.hasOwnProperty("mieng") && ten_bophan == "mieng") {
                        if (check_color.mieng == color) {
                            count_color += 1;
                        }
                    }
                    if (current_level == 3) {
                        if (check_color.hasOwnProperty("chan") && ten_bophan == "chan") {
                            if (check_color.chan == color) {
                                count_color += 1;
                            }
                        }
                        if (check_color.hasOwnProperty("tay") && ten_bophan == "tay") {
                            if (check_color.tay == color) {
                                count_color += 1;
                            }
                        }
                        if (check_color.hasOwnProperty("tai") && ten_bophan == "tai") {
                            if (check_color.tai == color) {
                                count_color += 1;
                            }
                        }
                        if (check_color.hasOwnProperty("carot") && ten_bophan == "carot") {
                            if (check_color.thanduoi == color) {
                                count_color += 1;
                            }
                        }
                    }
                }
                switch(count_color){
                    case 1: score = current_level + (count_color * 10); break;
                    case 2: score = current_level + (count_color * 15); break;
                    case 3: score = current_level + (count_color * 20); break;
                    case 4: score = current_level + (count_color * 25); break;
                    case 5: score = current_level + (count_color * 30); break;
                    case 6: score = current_level + (count_color * 35); break;
                    default: score = 0; break;
                }
                if (confirm("Bé tô đúng " + count_color + " màu. OK để lên cấp mới - Hủy để tô lại màu?")) {
                    let tang = parseInt(current_level) + 1;
                    localStorage.setItem("level", tang);
                    tangcap.visible = true;
                }
                if(current_level == 6){
                    end.visible = true;
                    end.setDepth(99999);
                    return;
                }
            }else{
                alert("Có vẻ bé chưa hoàn thiện phần tô màu nhỉ, vui lòng chọn màu để tô nhé!");
            }
        });

        let but_xanhnuocbien=this.add.image(50, 1260, "xanhnuocbien");
        let but_dacamnhat = this.add.image(120, 1250, "dacamnhat");
        let but_xanhlacaynhat=this.add.image(200, 1240, "xanhlacaynhat");
        let but_xanhlacaydam=this.add.image(300, 1220, "xanhlacaydam");
        let but_xanhlacay=this.add.image(400, 1220, "xanhlacay");
        let but_xanhtim=this.add.image(500, 1230, "xanhtim");

        let but_dacamdam = this.add.image(50, 1320, "dacamdam");
        let but_hongcaro = this.add.image(200, 1310, "hongcaro");
        let but_hongdam = this.add.image(300, 1310, "hongdam");
        let but_hongnhat = this.add.image(400, 1310, "hongnhat");
        let but_hongsao=this.add.image(490, 1310, "hongsao");

        let but_nau=this.add.image(50, 1380, "nau");
        let but_naudam=this.add.image(130, 1380, "naudam");
        let but_naunhat=this.add.image(200, 1380, "naunhat");
        let but_vang=this.add.image(300, 1380, "vang");
        let but_vangdam=this.add.image(400, 1380, "vangdam");
        let but_vangnghe=this.add.image(490, 1380, "vangnghe");



        let lstBut = [but_dacamdam, but_dacamnhat, but_hongcaro, but_hongdam, but_hongnhat, but_hongsao, but_nau, but_naudam, but_naunhat, but_vang, but_vangdam, but_vangnghe, but_xanhlacay, but_xanhlacaynhat, but_xanhlacaydam, but_xanhnuocbien, but_xanhtim, ];
        var lstFisrt = [];
        lstBut.forEach(function (item) {
            item.setScale(0.5);
            item.setInteractive({
                useHandCursor: true
            }).on("pointerdown", function () {
                if (frame_huongdan.visible) {
                    frame_huongdan.visible = false;
                    trogiup.visible = false;
                    trogiup.setDepth(0);
                    frame_huongdan.setDepth(0);
                }
                // Đặt opacity cho item đang chọn
                current_color = item.texture.key;
                let selectedColor = current_color;
                var current_index = 0;
                colors_selected.push(selectedColor);
                if (item.alpha == 1) {
                    item.setAlpha(0.2);
                    lstFisrt.push(item);
                } else {
                    if (colors_selected.length > 0) {
                        colors_selected.forEach(function (color) {
                            if (color == selectedColor) {
                                colors_selected.remove_by_value(color);
                            }
                        });
                    }
                    item.setAlpha(1);
                    lstFisrt.remove_by_value(item);
                }
                colors_selected = Array.from(new Set(colors_selected));
                if (colors_selected.length > 1) {
                    lstFisrt[current_index].setAlpha(1);
                    lstFisrt.shift();
                    colors_selected.shift();
                }
                // console.log(lstFisrt);
            });
        });


        // var rt = this.add.renderTexture(0, 0, 800, 600);

        // this.input.on('pointermove', function (pointer) {
    
        //     if (pointer.isDown)
        //     {
        //         get_current_color(colors_selected);
        //         console.log(tint_color_str);
        //         rt.draw('brush2', pointer.x - 32, pointer.y - 32, 1, tint_color);
        //     }
    
        // }, this);
     
    }
    form_load() {
        if (localStorage.getItem("frame_huongdan")) {
            frame_huongdan.visible = false;
        }
    }
    huychuong_btn_click() {
        if (current_level < 8) {
            current_level++;
            localStorage.setItem("level", current_level++);
            location.reload();
        }
    }
    back_click() {
        if (current_level > 1) {
            current_level--;
            localStorage.setItem("level", current_level);
            location.reload();
        }
    }
    home_click() {
        location.href = "../index.html";
        frame_huongdan.visible = true;
        plus.visible = false;
        frame_huongdan.setDepth(1);
    }
    load_background() {
        // Cài đặt background
        let form_background = this.add.image(this.cameras.main.width / 2, this.cameras.main.height / 2, 'background');
        let scaleX = this.cameras.main.width / form_background.width;
        let scaleY = this.cameras.main.height / form_background.height;
        let scale = Math.max(scaleX, scaleY);
        form_background.setScale(scale).setScrollFactor(0);
        form_background.setInteractive({
            useHandCursor: false
        }).on("pointerdown", this.form_load);
    }
    load_mp3() {

        // Tải nhạc
        right = this.sound.add('right');
        right1 = this.sound.add('right1');
        right2 = this.sound.add('right2');
        combo = this.sound.add('combo');
        wrong = this.sound.add('wrong');
        botbien = this.sound.add('botbien');
        // botbien.setLoop(true);
        // botbien.play();
    }
    load_image() {

        // Cài đặt hướng dẫn
        let help = this.add.image(50, 250, "help");
        help.setScale(1.3);
        help.setInteractive({
                useHandCursor: true
            })
            .on("pointerdown", function () {
                help.setScale(frame_huongdan.visible ? 1.3 : 1.5);
                frame_huongdan.visible = frame_huongdan.visible ? false : true;
                localStorage.setItem("frame_huongdan", frame_huongdan.visible);
                frame_huongdan.setDepth(10);
            })
            .on('pointerover', function () {
                help.setScale(1.5);
            })
            .on('pointerout', function () {
                help.setScale(1.3);
            });
        var correct_position = new Array();
        var check_point = 0;
        var right_complete = new Array();
        let obj_level = level_object[level[current_level]]; // babyshark, fathershark,.....
        let count_bophan = Object.values(obj_level).length;
        for (var i = 1; i <= count_bophan; i++) {
            let prop = obj_level[i];
            let name = prop[0];
            let x = prop[1];
            let y = prop[2];
            let xx = prop[3];
            let yy = prop[4];
            if (name == "bg"){
                continue;
            }
            let sprites = this.add.image(xx, yy, name);
            sprites.setDepth(1);
            sprites.setScale(0.5);
            if (name == "khung") {
                continue;
            }
            
            if (name == "hinhmau") {
                sprites.setInteractive({
                    useHandCursor: true
                }).on('pointerdown', function () {
                    if (sprites.x <= 100) {
                        sprites.setScale(1);
                        sprites.x = 250;
                        sprites.y = 1000;
                    } else {
                        sprites.setScale(0.5);
                        sprites.x = 100;
                        sprites.y = 1000;
                    }
                    sprites.setDepth(10);
                });
                continue;
            }
            //  Tạo action kéo thả
            
            sprites.setInteractive({
                useHandCursor: true
            }).on("pointerdown", function(){
                // Lấy màu đang chọn
                get_current_color(colors_selected)
                sprites.setTint(tint_color);
                let bophan = sprites.texture.key;
                check_color[bophan] = tint_color_str;
            });
            correct_position[sprites.texture.key] = new Array(x, y);
            this.input.setDraggable(sprites);
            this.input.on('dragstart', function (pointer, gameObject) {
                this.children.bringToTop(gameObject);
            }, this);
            this.input.on('drag', function (pointe, gameObject, dragX, dragY) {
                // if(colors_selected.toString()){
                //     hongsao.setDepth(99);
                // }
                frame_huongdan.visible = false;
                gameObject.x = dragX;
                gameObject.y = dragY;
                gameObject.setOrigin(0.5);
                for (let j = 1; j <= count_bophan; j++) {
                    let x = correct_position[gameObject.texture.key][0];
                    let y = correct_position[gameObject.texture.key][1];
                    // console.log("Vị trí đang kéo: " + Math.floor(dragX) + " - " + Math.floor(dragY));
                    // console.log("Vị trí mặc định: " +x + " - " + y);
                    get_current_color(colors_selected);
                    if ((Math.floor(dragX) == x && Math.floor(dragY) == y)) {
                        right_complete[gameObject.texture.key] = new Array("ok");
                        gameObject.x = x;
                        gameObject.y = y;
                        // console.log("Chính xác 100%");
                        // if(!right2.play()){
                        //     right2.play();
                        // }

                    } else if (
                        ((x - Math.floor(dragX) + 15) > 5 && (x - Math.floor(dragX) - 15) < 7) &&
                        ((y - Math.floor(dragY) + 15) > 5 && (y - Math.floor(dragY) - 15) < 7)
                    ) {
                        right_complete[gameObject.texture.key] = new Array("ok");
                        gameObject.x = x;
                        gameObject.y = y;
                        //console.log("Đúng tọa độ");
                        // if(!right2.play()){
                        //     right2.play();
                        // }
                        // return;
                    }
                }
                if (Object.keys(right_complete).length == 5) {
                    if (check_point < 1) {
                        check_point = 1;
                        if (check_point == 1) {
                            if (current_level == 8) {
                                current_level = 0;
                            }
                            //current_level++;
                            //localStorage.setItem("level", current_level);
                            combo.play();
                            //tangcap.visible = true;
                            // location.reload();
                        }
                    }
                }
            });
        }
        // End for
        frame_huongdan = this.add.image(400, 500, "huongdan");
        frame_huongdan.setScale(0.4);
        frame_huongdan.setDepth(1);
        frame_huongdan.visible = false;
    }
    load_default_button() {
        // Cài đặt nút home
        let home_btn = this.add.image(70, 70, "home");
        home_btn.displayWidth = 90;
        home_btn.displayHeight = 90;
        // Cài đặt nút back
        let back_btn = this.add.image(180, 70, "back");
        back_btn.displayWidth = 120;
        back_btn.displayHeight = 90;
        // Cài đặt hình nền của khung đếm điểm
        // let score_btn = this.add.image(400, 150, "score");
        // score_btn.displayWidth = 400;
        // score_btn.displayHeight = 250;
        // Cài đặt huy chương
        let huychuong_btn = this.add.image(720, 90, "huychuong");
        huychuong_btn.displayWidth = 140;
        huychuong_btn.displayHeight = 150;
        // Cài đặt nút tăng cấp
        tangcap = this.add.image(400, 100, "tangcap");
        tangcap.displayWidth = 300;
        tangcap.displayHeight = 120;
        tangcap.visible = false;
        tangcap.setDepth(1);
        scoreText = this.add.text(25, 150, "Điểm: 0", {
            fontSize: "32px",
            fill: "#FF030F",
            fontFamily: "Pony"
        });
        // levelText = this.add.text(320, 145, "Cấp độ " + current_level, {
        //     fontSize: "40px",
        //     fill: "#FDD691",
        //     fontFamily: "Pony"
        // });
        plus = this.add.text(580, 33, "+", {
            fontSize: "60px",
            fill: "#F5BC2E",
            fontFamily: "Pony"
        });
        huychuong_text = this.add.text(590, 60, "01", {
            fontSize: "60px",
            fill: "#F5BC2E",
            fontFamily: "Pony"
        });
        plus.visible = false;
        home_btn.setInteractive({
            useHandCursor: true
        }).on('pointerdown', this.home_click);
        huychuong_btn.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.huychuong_btn_click);
        back_btn.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.back_click);
        // Cài đặt nút END
        end = this.add.image(450, 600, "end");
        end.setScale(0.4);
        end.setDepth(999999);
        end.visible = false;
        end.setInteractive( { useHandCursor: true  }).on("pointerdown", this.home_click);
    }
}
Array.prototype.remove_by_value = function (val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === val) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
}

function resize() {
    var canvas = document.querySelector("canvas");
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var windowRatio = windowWidth / windowHeight;
    var gameRatio = game.config.width / game.config.height;
    if (windowRatio < gameRatio) {
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    } else {
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}