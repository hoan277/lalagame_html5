﻿var default_language = {
    "sProcessing": "Đang xử lý...",
    "sLengthMenu": "Đang xem _MENU_",
    "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
    "sInfo": "Đang xem từ _START_ đến _END_ của tổng _TOTAL_ bạn chơi",
    "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 bạn chơi",
    "sInfoFiltered": "(được lọc từ _MAX_ bạn chơi)",
    "sInfoPostFix": "",
    "sSearch": "Tìm kiếm:",
    "sUrl": "",
    "oPaginate": {
        "sFirst": "Đầu",
        "sPrevious": "Trước",
        "sNext": "Tiếp",
        "sLast": "Cuối"
    },
    "select": {
        "rows": "Đang chọn: %d"
    }
}
var link_api = 'http://183.81.35.24:7001';
var form, lblForm, lblHello, lblName, username, dangnhap, dangky, dangxuat, xephang, logged;
$(() => {
    $("#btnLogin").click(function () {
        let link = link_api +  "/User/LoginWebGame?userName=" + $("#name").val() + '&userPass=' + $("#pass").val();
        if ($("#name").val() != "" && $("#pass").val() != "") {
            $.ajax({
                url: link,
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    if (data == "error") {
                        alert("Tài khoản hoặc mật khẩu không đúng.");
                        return;
                    }
                    let txtName = data.userFullName || data.userName;
                    lblName.setText(txtName);
                    localStorage.setItem("username", txtName);
                    localStorage.setItem("userId",  data.userId);
                    $('.main').hide(1000, "swing");
                    dangnhap.visible = false;
                    dangky.visible = false;
                    xephang.visible = true;
                    dangxuat.visible = true;
                    logged = true;
                },
                error: function (error) {
                    alert('Lỗi máy chủ đăng nhập: ' + error);
                }
            });
        } else {
            alert("Vui lòng nhập đầy đủ thông tin tài khoản và mật khẩu");
        }
    });
    $("#btnRegister").click(function () {
        if ($("#name").val() != "" && $("#pass").val() != "" && $("#fullname").val() != "") {
            $.ajax({
                url: link_api + "/User/RegisterWebGame",
                method: "GET",
                data: {
                    userName: $("#name").val(),
                    userPass: $("#pass").val(),
                    userFullName: $("#fullname").val(),
                    userEmail: $("#name").val(),
                    userStatus: 'active',
                },
                success: function (data) {
                    
                    if (data.code == "error") {
                        alert(data.message);
                        return;
                    }
                    let txtName = data.result.userFullName || data.result.userName;
                    lblName.setText(txtName);
                    localStorage.setItem("username", txtName);
                    localStorage.setItem("userId",  data.result.userId);
                    $('.main').hide(1000, "swing");
                    dangnhap.visible = false;
                    dangky.visible = false;
                    xephang.visible = true;
                    dangxuat.visible = true;
                    logged = true;
                },
                error: function (xhr) {
                    alert(xhr.responseText);
                    console.log(xhr.responseText);
                },
            });
        } else {
            alert("Vui lòng nhập đầy đủ thông tin tài khoản và mật khẩu");
        }
    });
    $("#btnRecovery").click(function () {
        if ($("#name").val() != "") {
            let link = link_api + "/User/RecoverPass?userEmail=" + $("#name").val();
            $.ajax({
                url: link,
                method: "GET",
                success: function (data) {
                    if (data.code == "error") {
                        alert(data.message);
                        return;
                    }
                    alert("Vui lòng kiểm tra email để lấy mật khẩu mới!");
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                },
            });
        } else {
            alert("Vui lòng nhập đầy đủ thông tin");
        }
    });

    $(".form .close").click(function () {
        $(".main").hide(1000, "swing");
    });
    $(".xephang .close").click(function () {
        $(".xephang").hide(1000, "swing");
    });
    $("input").on('keypress',function(e) {
        if(e.which == 13) {
            $("#btnLogin").trigger('click');
        }
    });
    $("input[type=radio]").on('click',function(e) {
        let action = $('input[type=radio]:checked').val();
        switch (action) {
            case 'signup':
                $("#btnRegister").show();
                $("#btnLogin").hide();
                $("#btnRecovery").hide();
                break;
            case 'signin':
                $("#btnRegister").hide();
                $("#btnLogin").show();
                $("#btnRecovery").hide();
                break;
            case 'reset':
                $("#btnRegister").hide();
                $("#btnLogin").hide();
                $("#btnRecovery").show();
                break;
        }
    });
    $("#btnClose").click(function () {
        $("form#login").hide();
        lblForm.visible = false;
        form.visible = false;
    });
});

function ChangeFont() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/windows phone/i.test(userAgent) || /android/i.test(userAgent) || /webkit/i.test(userAgent)) {
        return "Pony";
    }
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "Alfa Slab One";
    }
    return "Pony";
}
var fontName = ChangeFont()
WebFontConfig = {
    google: {
        families: ["Fresca", "Fredoka One", "Alfa Slab One", "Coiny"]
    }
};
(function () {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();
let game;
var logo, tronmau, xephinh, babyshark, tomau, click;
window.onload = function () {
    let gameConfig = {
        width: 800,
        height: 1420,
        scene: playGame,
        backgroundColor: 0x222222
    }
    game = new Phaser.Game(gameConfig);
    window.focus()
    resize();
   // window.addEventListener("resize", resize, false);
}

class playGame extends Phaser.Scene {
    constructor() {
        super("PlayGame");
    }
    preload() {
        // Setup Image
        let lstImage = ["logo", "tronmau", "tomau", "babyshark", "xephinh", "dangnhap", "dangky", "dangxuat", "xephang", "bg_form"];
        lstImage.forEach(e => this.load.image(e, 'images/' + e + '.png'));
        this.load.image("background", 'images/background.png');
        // Setup mp3
        let lstMp3 = ["click"];
        lstMp3.forEach(e => this.load.audio(e, 'mp3/' + e + '.mp3', {
            instances: 1
        }));
        lblHello = this.add.text(100, 520, "Xin chào,", {
            fontSize: "40px",
            fill: "#75DAC2",
            fontFamily: fontName
        });
        lblName = this.add.text(300, 520, 'BÉ CHƯA ĐĂNG NHẬP', {
            fontSize: "40px",
            fill: "#F9AF59",
            fontFamily: fontName
        });
        lblForm = this.add.text(295, 200, 'ĐĂNG NHẬP', {
            fontSize: "40px",
            fill: "#F9AF59",
            fontFamily: fontName
        });
    }
    update() {

    }
    create() {
        click = this.sound.add('click');
        // Cài đặt background
        let form_background = this.add.image(this.cameras.main.width / 2, this.cameras.main.height / 2, 'background');
        let scaleX = this.cameras.main.width / form_background.width;
        let scaleY = this.cameras.main.height / form_background.height;
        let scale = Math.max(scaleX, scaleY);
        form_background.setScale(scale).setScrollFactor(0);
        lblHello = this.add.text(100, 520, "Xin chào,", {
            fontSize: "40px",
            fill: "#75DAC2",
            fontFamily: fontName
        });
        lblName = this.add.text(300, 520, 'BÉ CHƯA ĐĂNG NHẬP', {
            fontSize: "40px",
            fill: "#F9AF59",
            fontFamily: fontName
        });
        lblForm = this.add.text(295, 500, 'ĐĂNG NHẬP', {
            fontSize: "40px",
            fill: "#F9AF59",
            fontFamily: fontName
        });

        // Cài đặt nút logo
        let logo = this.add.image(400, 270, "logo");
        logo.setScale(0.6);
        // Cài đặt ảnh trộn màu
        let tronmau = this.add.image(150, 720, "tronmau");
        tronmau.setScale(0.6);
        // Cài đặt nút xếp hình
        let xephinh = this.add.image(320, 880, "xephinh");
        xephinh.setScale(0.6);
        // Cài đặt nút babyshark
        let babyshark = this.add.image(480, 780, "babyshark");
        babyshark.setScale(0.6);
        // Cài đặt nút tomau
        let tomau = this.add.image(680, 720, "tomau");
        tomau.setScale(0.6);

        // Cài đặt nút dangnhap
        dangnhap = this.add.image(300, 620, "dangnhap");
        dangnhap.setScale(0.5);
        // Cài đặt nút dangky
        dangky = this.add.image(520, 620, "dangky");
        dangky.setScale(0.5);
        // Cài đặt nút xếp hạng
        xephang = this.add.image(300, 620, "xephang");
        xephang.setScale(0.6);
        // Cài đặt nút dangxuat
        dangxuat = this.add.image(550, 620, "dangxuat");
        dangxuat.setScale(0.6);
        username = localStorage.getItem("username");
        logged = username == null ? false : true;
        // Cài đặt form
        form = this.add.image(400, 720, "bg_form");
        form.displayWidth = 800;
        form.displayHeight = 500;
        form.setDepth(1);

        if (logged) {
            // Đã đăng nhập
            dangnhap.visible = false;
            dangky.visible = false;
            xephang.visible = true;
            dangxuat.visible = true;
            lblName.setText(username);
            lblForm.visible = false;
            form.visible = false;
        } else {
            //Chưa đăng nhập
            lblForm.visible = false;
            dangnhap.visible = true;
            dangky.visible = true;
            xephang.visible = false;
            dangxuat.visible = false;
            lblName.setText("BÉ CHƯA ĐĂNG NHẬP");
            form.visible = false;
        }
        tronmau.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.tronmau_click).on('pointerover', function () {
            tronmau.setScale(0.65);
        }).on('pointerout', function () {
            tronmau.setScale(0.6);
        });
        xephinh.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.xephinh_click).on('pointerover', function () {
            xephinh.setScale(0.65);
        }).on('pointerout', function () {
            xephinh.setScale(0.6);
        });
        babyshark.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.babyshark_click).on('pointerover', function () {
            babyshark.setScale(0.65);
        }).on('pointerout', function () {
            babyshark.setScale(0.6);
        });
        tomau.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.tomau_click).on('pointerover', function () {
            tomau.setScale(0.65);
        }).on('pointerout', function () {
            tomau.setScale(0.6);
        });
        dangnhap.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.dangnhap_click).on('pointerover', function () {
            dangnhap.setScale(0.6);
        }).on('pointerout', function () {
            dangnhap.setScale(0.5);
        });
        dangky.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.dangky_click).on('pointerover', function () {
            dangky.setScale(0.6);
        }).on('pointerout', function () {
            dangky.setScale(0.5);
        });
        xephang.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.xephang_click).on('pointerover', function () {
            xephang.setScale(0.65);
        }).on('pointerout', function () {
            xephang.setScale(0.6);
        });
        dangxuat.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.dangxuat_click).on('pointerover', function () {
            dangxuat.setScale(0.65);
        }).on('pointerout', function () {
            dangxuat.setScale(0.6);
        });
        lblForm.setDepth(1);
    }
    form_load() {

    }
    dangnhap_click() {
        $('input#signin').prop("checked", true);
        show_login();
    }
    tronmau_click() {
        clear_data();
        localStorage.setItem("gameSlug","tron-mau");
        setTimeout(function () {
            location.href = "tronmau/index.html";
        }, 300);
}
    xephinh_click() {
        clear_data();
        localStorage.setItem("gameSlug","puzzle");
        setTimeout(function () {
            location.href = "puzzle/index.html";
        }, 300);
    }
    babyshark_click() {
        clear_data();
        localStorage.setItem("gameSlug","babyshark");
        setTimeout(function () {
            location.href = "babyshark/index.html";
        }, 300);
    }
    tomau_click() {
        clear_data();
        localStorage.setItem("gameSlug","to-mau");
        setTimeout(function () {
            location.href = "tomau/index.html";
        }, 300);
    }

    dangky_click() {
        $('input[value=signup]').prop("checked", true);
        $(".main").show(1000, "swing");
    }
    xephang_click() {
        let list_user = [];
        let link_get_user = link_api + "/User/Get";
        let ajax_user = $.ajax({
            url: link_get_user,
            dataType: 'json',
            method:"GET",
            success: function(data){
                list_user = data.data;
            },
            error: function (xhr) {
                alert("Lỗi lấy dữ liệu người dùng.");
            },
        });
        let link_score = link_api + "/Score/Get";
        $.when(ajax_user).done(function(){
            $.ajax({
                url: link_score,
                type: 'GET',
                dataType: 'json',
                success: function(data){
                    if(data.data){
                        $('#xephang').DataTable().clear().destroy();
                        $('#xephang').DataTable({
                            "data": data.data,
                            "filter": true,
                            "lengthMenu": [[1,5,10, 25, 50, 100, 200, -1], [1,5,10, 25, 50, 100, 200, "All"]],
                            "pageLength": 10,
                            "rowId": "scoreId",
                            "language": default_language,
                            "dom": '<"top row"if<"clear">>rt<"bottom row"lp<"clear">>',
                            "columns": [
                                { data: "gameSlug" }
                                ,{
                                    data: null, render: function (data, type, row) {
                                        for(let i = 0; i< list_user.length;i++){
                                            if(list_user[i].userId == row.userId){
                                                return list_user[i].userFullName;        
                                            }
                                        }
                                        return "";
                                    }
                                }
                                ,{ data: "gameLevel" }
                                ,{ data: "gameScore" }
                                ,{
                                    data: null, render: function (data, type, row) {
                                        return new Date(row.scoreCreated.replace('T', ' ')).toLocaleString("vi-VN");
                                    }
                                }
                            ]
                        });
                    }
                },
                error: function(xhr){
                    alert('Lỗi lấy dữ liệu bảng xếp hạng');
                },
            });
            $(".xephang").show(700, "swing");
        });
    }
    dangxuat_click() {
        if (!confirm("Bạn có chắc muốn thoát tài khoản này không?")) {
            return false;
        }
        clear_data();
        localStorage.removeItem("username");
        location.reload();
    }

}

function show_login() {
    // $("#login").show();
    // form.visible = true;
    // form.setDepth(0);
    // lblForm.setText('ĐĂNG NHẬP');
    // lblForm.visible = true;
    // lblForm.setDepth(1);
    $(".main").show();
}

function clear_data() {
    click.play();
    localStorage.removeItem("begin");
    localStorage.removeItem("level");
    localStorage.removeItem("frame_huongdan");
    localStorage.removeItem("begin");
}

function resize() {
    var canvas = document.querySelector("canvas");
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var windowRatio = windowWidth / windowHeight;
    var gameRatio = game.config.width / game.config.height;
    if (windowRatio < gameRatio) {
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
        // let h = ((windowWidth / gameRatio) / 2);
        // $("form#login").css("width", ((windowWidth/2) + 100) );
        // $("form#login").css("top", (h + 20));
        // console.log("windowWidth: " + windowWidth);
        // console.log("(windowWidth / gameRatio) : " + (windowWidth / gameRatio));
    } else {
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
        // console.log("(windowHeight * gameRatio): " + (windowHeight * gameRatio));
        // console.log("windowHeight : " + windowHeight);
    }
    $(".xephang").css("width", $("canvas").width());
    $(".form").css("width", $("canvas").width() - 50);
}