﻿function update_score(slug, user, level, point){
    if (!navigator.onLine) { return; }
    let objScore = {gameSlug: slug, userId: user, gameLevel: level, gameScore: point}
    $.ajax({
        url:"http://183.81.35.24:7001/Score/Update",
        method:"POST",
        data:{
            score: objScore
        },
        success: function(data){
            console.log(data);
        },
        error: function(xhr){
            console.log("Lỗi: " + xhr.resposeText);
        },
    });
}
//update_score("puzzle", localStorage.getItem("userId"), current_level, score);
function ChangeFont() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
      if (/windows phone/i.test(userAgent) || /android/i.test(userAgent) || /webkit/i.test(userAgent)) {
          return "Pony";
      }
      if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
          return "Alfa Slab One";
      }
      return "Pony";
  }
var fontName = ChangeFont()
WebFontConfig = {
    google: { families: ["Fresca","Fredoka One","Alfa Slab One","Coiny"] }
    };
    (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
    })();
let game;
let gameOptions = {
    gemSize: 100,
    swapSpeed: 200,
    fallSpeed: 100,
    destroySpeed: 200,
    boardOffset: {
        x: 100,
        y: 430
    }
}
var first_click = 0;
// Khai báo biến ảnh, âm thanh
   var lstNangLuong = new Array();
   var score = 0,count_move=30; check_combo=0;
   var scoreText, levelText, moveText, huychuong_text;
   var win,failed,right, right1, right2, combo, wrong, botbien,click;
   var frame_huongdan,start_btn,timeout_btn,play_again,timeout, tangcap, end;
   var current_level = localStorage.getItem("level") || 1;
   var plus;
   var level = {
        1: 100,
        2: 120,
        3: 140,
        4: 160,
        5: 180,
        6: 200,
    }
    var level_time = {
        1: 7000,
        2: 6000,
        3: 5000,
        4: 4000,
        5: 3000,
        6: 2500,
    }
    var count_move ={
        1: 40,
        2: 35,
        3: 30,
        4: 25,
        5: 20,
        6: 15,
    }
   var current_move = 0;
   var thanhNangLuong = 20;
   var check_timeout = false;
   
window.onload = function () {
    let gameConfig = {
        width: 800,
        height: 1420,
        scene: playGame,
        backgroundColor: 0x222222
    }
    game = new Phaser.Game(gameConfig);
    window.focus()
    resize();
    window.addEventListener("resize", resize, false);
}
var check_update = 0;
setInterval(function () {
    if(localStorage.getItem("begin") != null){
        if(check_timeout){
            check_timeout = false;
        }
        // console.log(lstNangLuong);
        if (thanhNangLuong >= 0 ) {
            if(lstNangLuong[thanhNangLuong])
            {
                lstNangLuong[thanhNangLuong].visible = false;
                thanhNangLuong--;
            }
        }
        if(thanhNangLuong < 0){
            check_update++;
            if (check_update == 1) {
                // Check có đủ điểm để qua level mới hay không?
                if(score < level[current_level]){
                    timeout_btn.visible = true;
                    play_again.visible = true;
                    timeout_btn.setDepth(1);
                    play_again.setDepth(1);
                    failed.play();
                }else{
                    tangcap.visible = true;
                    win.play();
                    localStorage.removeItem("begin");
                    localStorage.setItem("level", current_level < 5 ? + current_level+1: 1);
                    setTimeout(function(){location.reload();},2000);
                }
                update_score("babyshark", localStorage.getItem("userId"), current_level, score);
                if(current_level == 5){
                    end.visible = true;
                }
            }
        }
        
    }
}, level_time[current_level]);
//score += 10;scoreText.setText("Score: " + score);
class playGame extends Phaser.Scene{
    constructor(){
        super("PlayGame");
    }
    preload(){
        // Setup mp3
        let lstMp3 = ["win", "failed", "right", "right1", "right2", "combo", "wrong", "botbien","click"];
        lstMp3.forEach(e => this.load.audio(e, '../mp3/' + e + '.mp3', {instances: 1}));
        // Setup Image
        let lstImage = ["huongdan", "khung","play_again", "start", "power", "tangcap", "nangLuong",  "home", "back", "huychuong", "score", "timeout","correct","end"];
        lstImage.forEach(e => this.load.image(e, 'images/' + e + '.png'));
        this.load.image("background",'images/background-lv' + current_level + '.png');
        this.load.spritesheet("gems", "images/sprites.png", {
            frameWidth: gameOptions.gemSize,
            frameHeight: gameOptions.gemSize
        });
        scoreText = this.add.text(380, 230, "0", {fontSize: "50px",fill: "#9d6727",fontFamily:fontName});
        levelText = this.add.text(210, 235, current_level, {fontSize: "40px",fill: "#fefd8d",fontFamily:fontName});
        moveText = this.add.text(570, 230, count_move[current_level], {fontSize: "40px",fill: "#b5711f",fontFamily:fontName});
        plus = this.add.text(580, 33, "+", {fontSize: "60px",fill: "#F5BC2E",fontFamily:fontName});
        huychuong_text = this.add.text(590, 60, "01", {fontSize: "60px",fill: "#F5BC2E",fontFamily:"Coiny"});
    }
    update(){
        if(tangcap.visible){
            if (tangcap.y > -50){
                console.log(tangcap.y--);
                tangcap.y = tangcap.y--;
            }
        }
    }
    create() {
        // Tải nhạc

        timeout = this.sound.add('timeout');
        right = this.sound.add('right');
        win = this.sound.add('win'); 
        failed = this.sound.add('failed');
        right1 = this.sound.add('right1');
        right2 = this.sound.add('right2');
        combo = this.sound.add('combo');
        wrong = this.sound.add('wrong');
        botbien = this.sound.add('botbien');
        click = this.sound.add('click');
        botbien.setLoop(true);
        botbien.play();
        // Cài đặt background
        let form_background = this.add.image(this.cameras.main.width / 2, this.cameras.main.height / 2, 'background');
        let scaleX = this.cameras.main.width / form_background.width;
        let scaleY = this.cameras.main.height / form_background.height;
        let scale = Math.max(scaleX, scaleY);
        form_background.setScale(scale).setScrollFactor(0);
        // Cài đặt khung game 6 ngang 6 dọc
        this.match3 = new Match3({
            rows: 6,
            columns: 6,
            items: 6
        });
        // Cài đặt nút home
        let home_btn = this.add.image(70,70, "home");
        home_btn.displayWidth = 90;
        home_btn.displayHeight = 90;
        // Cài đặt ảnh hết giờ
        timeout_btn = this.add.image(380,600, "timeout");
        timeout_btn.displayWidth = 800;
        timeout_btn.displayHeight = 700;
        timeout_btn.visible = false;
        // Cài đặt nút chơi lại
        play_again= this.add.image(430,820, "play_again");
        play_again.displayWidth = 300;
        play_again.displayHeight = 120;
        play_again.visible = false;
        // Cài đặt nút tăng cấp
        tangcap = this.add.image(400,100, "tangcap");
        tangcap.displayWidth = 300;
        tangcap.displayHeight = 120;
        tangcap.visible = false;
        tangcap.setDepth(1);
        // Cài đặt nút back
        let back_btn = this.add.image(180,70, "back");
        back_btn.displayWidth = 120;
        back_btn.displayHeight = 90;
        // Cài đặt hình nền của các con cá
        let khung_btn = this.add.image(400,720, "khung");
        khung_btn.displayWidth = 670;
        khung_btn.displayHeight = 670;
        // Cài đặt hình nền của khung đếm điểm
        let score_btn = this.add.image(400,220, "score");
        score_btn.displayWidth = 640;
        score_btn.displayHeight = 220;
        // Cài đặt hình nền của khung năng lượng
        let power_btn = this.add.image(400,360, "power");
        power_btn.displayWidth = 635;
        power_btn.displayHeight = 100;
        
        // Cài đặt huy chương
        let huychuong_btn = this.add.image(720,90, "huychuong");
        huychuong_btn.displayWidth = 140;
        huychuong_btn.displayHeight = 150;
        // Cài đặt nút END
        end = this.add.image(450, 600, "end");
        end.setScale(0.4);
        end.setDepth(99);
        end.visible = false;
        end.setInteractive( { useHandCursor: true  }).on("pointerdown", this.home_click);

        // Cài đặt hình nền của thanh năng lượng
        var defaultX = 195;
        for(let i=0;i<=thanhNangLuong;i++){
            let nangLuong_btn = this.add.image(defaultX,362, "nangLuong");
            nangLuong_btn.displayWidth = 65;
            nangLuong_btn.displayHeight = 85;
            defaultX =  defaultX + 24.6;
            lstNangLuong.push(nangLuong_btn);
        }
        // Đếm lượt move theo 
        scoreText = this.add.text(380, 230, "0", {fontSize: "50px",fill: "#9d6727",fontFamily:fontName});
        levelText = this.add.text(210, 235, current_level, {fontSize: "40px",fill: "#fefd8d",fontFamily:fontName});
        moveText = this.add.text(570, 230, count_move[current_level], {fontSize: "40px",fill: "#b5711f",fontFamily:fontName});
        plus = this.add.text(580, 33, "+", {fontSize: "60px",fill: "#F5BC2E",fontFamily:fontName});
        huychuong_text = this.add.text(590, 60, "01", {fontSize: "60px",fill: "#F5BC2E",fontFamily:"Coiny"});

        plus.visible = false;
        this.match3.generateField();
        this.canPick = true;
        this.dragging = false;
        this.drawField();
        this.input.on("pointerdown", this.gemSelect, this);
        home_btn.setInteractive( { useHandCursor: true  }).on('pointerdown', this.home_click);
        form_background.setInteractive( { useHandCursor: false  }).on("pointerdown", this.form_load);
        huychuong_btn.setInteractive( { useHandCursor: true  }).on("pointerdown", this.huychuong_btn_click);
        back_btn.setInteractive( { useHandCursor: true  }).on("pointerdown", this.back_click);
        play_again.setInteractive( { useHandCursor: true  }).on("pointerdown", this.play_again_click);
        frame_huongdan = this.add.image(400,565, "huongdan");
        frame_huongdan.displayWidth = 800;
        frame_huongdan.displayHeight = 800;
        start_btn = this.add.image(560,840, "start");
        start_btn.displayWidth = 330;
        start_btn.displayHeight = 230;
        start_btn.setInteractive( { useHandCursor: true  }).on("pointerdown", this.start_click);
        console.log(localStorage.getItem("frame_huongdan") );
        if(localStorage.getItem("frame_huongdan") == "true" ||  localStorage.getItem("frame_huongdan") == null){
            frame_huongdan.visible = true;
            
        }else{
            frame_huongdan.visible = false;
            start_btn.y = start_btn.y + 280;
        }
        start_btn.visible = true;
        localStorage.removeItem("begin");
        document.getElementsByTagName("canvas")[0].classList.add("not_allowed");
        current_move = count_move[current_level];
    }
    form_load(){
        // console.log(localStorage.getItem("frame_huongdan"));
        // if(localStorage.getItem("frame_huongdan")){
        //     frame_huongdan.visible = false;
        //     start_btn.visible = false;
        // }
    }
    
    huychuong_btn_click(){
        if(current_level < 6){
            console.log(current_level++);
            localStorage.setItem("level",current_level++);
            location.reload();
        }
    }
    back_click(){
        if(current_level > 1){
            console.log(current_level--);
            localStorage.setItem("level",current_level--);
            location.reload();
        }
    }
    play_again_click(){
        localStorage.removeItem("begin");
        // localStorage.removeItem("frame_huongdan");
        location.reload();
    }
    start_click(){
        localStorage.setItem("begin","start");
        frame_huongdan.visible = false;
        start_btn.visible = false;
        localStorage.setItem("frame_huongdan", false);
        document.getElementsByTagName("canvas")[0].classList.remove("not_allowed");
        console.log(lstNangLuong);
    }
    home_click(){
        location.href = "../index.html";
        // frame_huongdan.visible = true;
        // start_btn.visible = true;
        // plus.visible = false;
        // frame_huongdan.setDepth(1);
        // start_btn.setDepth(1);
        // if(start_btn.y >= 1020)
        //     start_btn.y = start_btn.y - 280;
    }
    drawField(){
        this.poolArray = [];
        for(let i = 0; i < this.match3.getRows(); i ++){
            for(let j = 0; j < this.match3.getColumns(); j ++){
                let gemX = gameOptions.boardOffset.x + gameOptions.gemSize * j + gameOptions.gemSize / 2;
                let gemY = gameOptions.boardOffset.y + gameOptions.gemSize * i + gameOptions.gemSize / 2;
                let gem = this.add.sprite(gemX, gemY, "gems", this.match3.valueAt(i, j));
                this.match3.setCustomData(i, j, gem);
            }
        }
    }
    gemSelect(pointer) {
        if(this.canPick && localStorage.getItem("begin")  != null){
            this.dragging = true;
            let row = Math.floor((pointer.y - gameOptions.boardOffset.y) / gameOptions.gemSize);
            let col = Math.floor((pointer.x - gameOptions.boardOffset.x) / gameOptions.gemSize);
            if(this.match3.validPick(row, col)){
                let selectedGem = this.match3.getSelectedItem();
                if(!selectedGem){
                    this.match3.customDataOf(row, col).setScale(1.2);
                    // this.match3.customDataOf(row, col).setDepth(1);
                    this.match3.setSelectedItem(row, col);
                    console.log("Selected\nrow:" + row +"\ncol:"+col);
                    click.play();
                    //console.log(this.match3);
                    // shadow.tint = 0x000000;
                    // shadow.alpha = 0.6;
                    this.match3.customDataOf(row, col).setTint(0xFFFFFF);
                }
                else {
                    // console.log("Move");
                    if(this.match3.areTheSame(row, col, selectedGem.row, selectedGem.column)){
                        this.match3.customDataOf(row, col).setScale(1);
                        this.match3.deleselectItem();
                    }
                    else {
                        if (this.match3.areNext(row, col, selectedGem.row, selectedGem.column)) {
                            this.match3.customDataOf(selectedGem.row, selectedGem.column).setScale(1);
                            this.match3.deleselectItem();
                            this.swapGems(row, col, selectedGem.row, selectedGem.column, true);
                        }
                        else{
                            this.match3.customDataOf(selectedGem.row, selectedGem.column).setScale(1);
                            this.match3.customDataOf(row, col).setScale(1.2);
                            this.match3.setSelectedItem(row, col);
                        }
                    }
                }
            }
            
        }
    }
    swapGems(row, col, row2, col2, swapBack){
        // check số move còn lại
        console.log(current_move);
        console.log(current_move < 0);
        if(current_move < 1){
            for(let i=0;i<=20;i++){
                lstNangLuong[thanhNangLuong].visible = false;
                thanhNangLuong--;
                if(thanhNangLuong < 0){
                    check_timeout = true;
                    // Check có đủ điểm để qua level mới hay không?
                    if(score < level[current_level]){
                        timeout_btn.visible = true;
                        play_again.visible = true;
                        timeout_btn.setDepth(1);
                        play_again.setDepth(1);
                        failed.play();
                    }else{
                        tangcap.visible = true;
                        win.play();
                        localStorage.removeItem("begin");
                        localStorage.setItem("level", current_level < 5 ? + current_level+1: 1);
                        setTimeout(function(){location.reload();},2000);
                    }
                }
            }
            return false;
        }
        let movements = this.match3.swapItems(row, col, row2, col2);
        this.swappingGems = 2;
        this.canPick = false;
        check_combo = 0;
        movements.forEach(function(movement){
            this.tweens.add({
                targets: this.match3.customDataOf(movement.row, movement.column),
                x: this.match3.customDataOf(movement.row, movement.column).x + gameOptions.gemSize * movement.deltaColumn,
                y: this.match3.customDataOf(movement.row, movement.column).y + gameOptions.gemSize * movement.deltaRow,
                duration: gameOptions.swapSpeed,
                callbackScope: this,
                onComplete: function(){
                    this.swappingGems --;
                    if(this.swappingGems == 0){
                        if(!this.match3.matchInBoard()){
                            if(swapBack){
                                console.log("swapBack");
                                this.swapGems(row, col, row2, col2, false);
                            }
                            else {
                                console.log("Sai roi nhe");
                                this.canPick = true;
                                this.sound.play('wrong');
                                if(current_move > 0){
                                    current_move--;
                                    moveText.setText(current_move);
                                }
                            }
                        }
                        else{
                            console.log(current_move);
                            if(current_move > 0){
                                current_move--;
                                moveText.setText(current_move);
                            }
                            this.handleMatches();
                            score += 10;
                            scoreText.setText(score);
                            scoreText.x = score > 100 ? 360 : 370;
                            console.log("Dung roi day");
                            this.sound.play('right2');
                            // console.log(current_level);
                            switch(current_level){
                                case 1:
                                    if(score == level["1"]){
                                        // levelText.setText("2");
                                        this.sound.play('combo');
                                        // current_level = 2;
                                        plus.visible = true;
                                        // localStorage.setItem("level",2);
                                    }
                                case 2:
                                    if(score == level["2"]){
                                        // levelText.setText("3");
                                        this.sound.play('combo');
                                        // current_level = 3;
                                        plus.visible = true;
                                        // localStorage.setItem("level",3);
                                    }
                                case 3:
                                    if(score == level["3"]){
                                        // levelText.setText("4");
                                        this.sound.play('combo');
                                        // current_level = 4;
                                        plus.visible = true;
                                        // localStorage.setItem("level",4);
                                    }
                                case 4:
                                    if(score == level["4"]){
                                        // levelText.setText("5");
                                        this.sound.play('combo');
                                        // current_level = 5;
                                        plus.visible = true;
                                        // localStorage.setItem("level",5);
                                    }
                                case 5:
                                    if(score == level["5"]){
                                        this.sound.play('combo');
                                        localStorage.setItem("level",1);
                                    }
                            }
                            
                        }
                    }
                }
            })
        }.bind(this))
    }
    handleMatches(){
        let gemsToRemove = this.match3.getMatchList();
        let destroyed = 0;
        gemsToRemove.forEach(function(gem){
            this.poolArray.push(this.match3.customDataOf(gem.row, gem.column))
            destroyed ++;
            this.tweens.add({
                targets: this.match3.customDataOf(gem.row, gem.column),
                alpha: 0,
                duration: gameOptions.destroySpeed,
                callbackScope: this,
                onComplete: function(event, sprite){
                    destroyed --;
                    if(destroyed == 0){
                        this.makeGemsFall();
                    }
                }
            });
        }.bind(this));
        // Check Combo
        check_combo++;
        if(check_combo >= 2){
            score = score + (10 * check_combo);
            scoreText.setText(score);
            this.sound.play("combo");
            // reset combo
            check_combo = 0;
        }
    }
    makeGemsFall(){
        let moved = 0;
        this.match3.removeMatches();
        let fallingMovements = this.match3.arrangeBoardAfterMatch();
        fallingMovements.forEach(function(movement){
            moved ++;
            this.tweens.add({
                targets: this.match3.customDataOf(movement.row, movement.column),
                y: this.match3.customDataOf(movement.row, movement.column).y + movement.deltaRow * gameOptions.gemSize,
                duration: gameOptions.fallSpeed * Math.abs(movement.deltaRow),
                callbackScope: this,
                onComplete: function(){
                    moved --;
                    if(moved == 0){
                        this.endOfMove()
                    }
                }
            })
        }.bind(this));
        let replenishMovements = this.match3.replenishBoard();
        replenishMovements.forEach(function(movement){
            moved ++;
            let sprite = this.poolArray.pop();
            sprite.alpha = 1;
            sprite.y = gameOptions.boardOffset.y + gameOptions.gemSize * (movement.row - movement.deltaRow + 1) - gameOptions.gemSize / 2;
            sprite.x = gameOptions.boardOffset.x + gameOptions.gemSize * movement.column + gameOptions.gemSize / 2,
            sprite.setFrame(this.match3.valueAt(movement.row, movement.column));
            this.match3.setCustomData(movement.row, movement.column, sprite);
            this.tweens.add({
                targets: sprite,
                y: gameOptions.boardOffset.y + gameOptions.gemSize * movement.row + gameOptions.gemSize / 2,
                duration: gameOptions.fallSpeed * movement.deltaRow,
                callbackScope: this,
                onComplete: function(){
                    moved --;
                    if(moved == 0){
                        this.endOfMove()
                    }
                }
            });
        }.bind(this))
    }
    endOfMove(){
        if(this.match3.matchInBoard()){
            this.time.addEvent({
                delay: 250,
                callback: this.handleMatches()
            });
        }
        else{
            this.canPick = true;
            this.selectedGem = null;
        }
    }
}

class Match3{

    // constructor, simply turns obj information into class properties
    constructor(obj){
        this.rows = obj.rows;
        this.columns = obj.columns;
        this.items = obj.items;
    }

    // generates the game field
    generateField(){
        this.gameArray = [];
        this.selectedItem = false;
        for(let i = 0; i < this.rows; i ++){
            this.gameArray[i] = [];
            for(let j = 0; j < this.columns; j ++){
                do{
                    let randomValue = Math.floor(Math.random() * this.items);
                    this.gameArray[i][j] = {
                        value: randomValue,
                        isEmpty: false,
                        row: i,
                        column: j
                    }
                } while(this.isPartOfMatch(i, j));
            }
        }
    }
    // returns true if there is a match in the board
    matchInBoard(){
        for(let i = 0; i < this.rows; i ++){
            for(let j = 0; j < this.columns; j ++){
                if (this.isPartOfMatch(i, j)) {
                    
                    return true;
                }
            }
        }
        return false;
    }

    // returns true if the item at (row, column) is part of a match
    isPartOfMatch(row, column) {
        return this.isPartOfHorizontalMatch(row, column) || this.isPartOfVerticalMatch(row, column);
    }

    // returns true if the item at (row, column) is part of an horizontal match
    isPartOfHorizontalMatch(row, column){
        return this.valueAt(row, column) === this.valueAt(row, column - 1) && this.valueAt(row, column) === this.valueAt(row, column - 2) ||
                this.valueAt(row, column) === this.valueAt(row, column + 1) && this.valueAt(row, column) === this.valueAt(row, column + 2) ||
                this.valueAt(row, column) === this.valueAt(row, column - 1) && this.valueAt(row, column) === this.valueAt(row, column + 1);
    }

    // returns true if the item at (row, column) is part of an horizontal match
    isPartOfVerticalMatch(row, column){
        return this.valueAt(row, column) === this.valueAt(row - 1, column) && this.valueAt(row, column) === this.valueAt(row - 2, column) ||
                this.valueAt(row, column) === this.valueAt(row + 1, column) && this.valueAt(row, column) === this.valueAt(row + 2, column) ||
                this.valueAt(row, column) === this.valueAt(row - 1, column) && this.valueAt(row, column) === this.valueAt(row + 1, column)
    }

    // returns the value of the item at (row, column), or false if it's not a valid pick
    valueAt(row, column){
        if(!this.validPick(row, column)){
            return false;
        }
        return this.gameArray[row][column].value;
    }

    // returns true if the item at (row, column) is a valid pick
    validPick(row, column){
        return row >= 0 && row < this.rows && column >= 0 && column < this.columns && this.gameArray[row] != undefined && this.gameArray[row][column] != undefined;
    }

    // returns the number of board rows
    getRows(){
        return this.rows;
    }

    // returns the number of board columns
    getColumns(){
        return this.columns;
    }

    // sets a custom data on the item at (row, column)
    setCustomData(row, column, customData){
        this.gameArray[row][column].customData = customData;
    }

    // returns the custom data of the item at (row, column)
    customDataOf(row, column){
        return this.gameArray[row][column].customData;
    }

    // returns the selected item
    getSelectedItem(){
        return this.selectedItem;
    }

    // set the selected item as a {row, column} object
    setSelectedItem(row, column){
        this.selectedItem = {
            row: row,
            column: column
        }
    }

    // deleselects any item
    deleselectItem(){
        this.selectedItem = false;
    }

    // checks if the item at (row, column) is the same as the item at (row2, column2)
    areTheSame(row, column, row2, column2){
        return row == row2 && column == column2;
    }

    // returns true if two items at (row, column) and (row2, column2) are next to each other horizontally or vertically
    areNext(row, column, row2, column2){
        return Math.abs(row - row2) + Math.abs(column - column2) == 1;
    }

    // swap the items at (row, column) and (row2, column2) and returns an object with movement information
    swapItems(row, column, row2, column2){
        let tempObject = Object.assign(this.gameArray[row][column]);
        this.gameArray[row][column] = Object.assign(this.gameArray[row2][column2]);
        this.gameArray[row2][column2] = Object.assign(tempObject);
        return [{
            row: row,
            column: column,
            deltaRow: row - row2,
            deltaColumn: column - column2
        },
        {
            row: row2,
            column: column2,
            deltaRow: row2 - row,
            deltaColumn: column2 - column
        }]
    }

    // return the items part of a match in the board as an array of {row, column} object
    getMatchList(){
        let matches = [];
        for(let i = 0; i < this.rows; i ++){
            for(let j = 0; j < this.columns; j ++){
                if (this.isPartOfMatch(i, j)) {
                    matches.push({
                        row: i,
                        column: j
                    });
                }
            }
        }
        return matches;
    }

    // removes all items forming a match
    removeMatches(){
        let matches = this.getMatchList();
        
        matches.forEach(function(item){
            this.setEmpty(item.row, item.column)
        }.bind(this))
    }

    // set the item at (row, column) as empty
    setEmpty(row, column){
        this.gameArray[row][column].isEmpty = true;
    }

    // returns true if the item at (row, column) is empty
    isEmpty(row, column){
        return this.gameArray[row][column].isEmpty;
    }

    // returns the amount of empty spaces below the item at (row, column)
    emptySpacesBelow(row, column){
        let result = 0;
        if(row != this.getRows()){
            for(let i = row + 1; i < this.getRows(); i ++){
                if(this.isEmpty(i, column)){
                    result ++;
                }
            }
        }
        return result;
    }

    // arranges the board after a match, making items fall down. Returns an object with movement information
    arrangeBoardAfterMatch(){
        let result = []
        for(let i = this.getRows() - 2; i >= 0; i --){
            for(let j = 0; j < this.getColumns(); j ++){
                let emptySpaces = this.emptySpacesBelow(i, j);
                if(!this.isEmpty(i, j) && emptySpaces > 0){
                    this.swapItems(i, j, i + emptySpaces, j)
                    result.push({
                        row: i + emptySpaces,
                        column: j,
                        deltaRow: emptySpaces,
                        deltaColumn: 0
                    });
                }
            }
        }
        return result;
    }

    // replenished the board and returns an object with movement information
    replenishBoard(){
        let result = [];
        for(let i = 0; i < this.getColumns(); i ++){
            if(this.isEmpty(0, i)){
                let emptySpaces = this.emptySpacesBelow(0, i) + 1;
                for(let j = 0; j < emptySpaces; j ++){
                    let randomValue = Math.floor(Math.random() * this.items);
                    result.push({
                        row: j,
                        column: i,
                        deltaRow: emptySpaces,
                        deltaColumn: 0
                    });
                    this.gameArray[j][i].value = randomValue;
                    this.gameArray[j][i].isEmpty = false;
                }
            }
        }
        return result;
    }
}

function resize() {
    var canvas = document.querySelector("canvas");
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var windowRatio = windowWidth / windowHeight;
    var gameRatio = game.config.width / game.config.height;
    if(windowRatio < gameRatio){
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else{
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}

class OutlinePipeline extends Phaser.Renderer.WebGL.Pipelines.TextureTintPipeline {
    constructor(game = Phaser.Game) {
        let config = {game: game,
            renderer: game.renderer,
            fragShader: `
            precision mediump float;
            uniform sampler2D uMainSampler;
            varying vec2 outTexCoord;
            void main(void) {
                vec4 color = texture2D(uMainSampler, outTexCoord);
                vec4 colorU = texture2D(uMainSampler, vec2(outTexCoord.x, outTexCoord.y - 0.001));
                vec4 colorD = texture2D(uMainSampler, vec2(outTexCoord.x, outTexCoord.y + 0.001));
                vec4 colorL = texture2D(uMainSampler, vec2(outTexCoord.x + 0.001, outTexCoord.y));
                vec4 colorR = texture2D(uMainSampler, vec2(outTexCoord.x - 0.001, outTexCoord.y));
                
                gl_FragColor = color;
                
                if (color.a == 0.0 && (colorU.a != 0.0 || colorD.a != 0.0 || colorL.a != 0.0 || colorR.a != 0.0)  ) {
                    gl_FragColor = vec4(1.0, 0.0, 0.0, .2);
                }
            }`};
        super(config);
    }
}