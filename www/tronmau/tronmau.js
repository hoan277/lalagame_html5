﻿function update_score(slug, user, level, point){
    if (!navigator.onLine) { return; }
    let objScore = {gameSlug: slug, userId: user, gameLevel: level, gameScore: point}
    $.ajax({
        url:"http://183.81.35.24:7001/Score/Update",
        method:"POST",
        data:{
            score: objScore
        },
        success: function(data){
            console.log(data);
        },
        error: function(xhr){
            console.log("Lỗi: " + xhr.resposeText);
        },
    });
}
//update_score("puzzle", localStorage.getItem("userId"), current_level, score);
function ChangeFont() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/windows phone/i.test(userAgent) || /android/i.test(userAgent) || /webkit/i.test(userAgent)) {
        return "Pony";
    }
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "Alfa Slab One";
    }
    return "Pony";
}
var fontName = ChangeFont()
WebFontConfig = {
    google: {
        families: ["Fresca", "Fredoka One", "Alfa Slab One", "Coiny"]
    }
};
(function () {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();
let game;
// Khai báo biến ảnh, âm thanh
var colors_selected = [];
var current_color = "";
var current_tuong = "";
var lstNangLuong = new Array();
var score = 0,
    count_move = 30;
check_combo = 0;
var scoreText, levelText, moveText, huychuong_text, questionsText;
var win, failed, right, right1, right2, combo, wrong, botbien, click;
var frame_huongdan,trogiup, start_btn, timeout_btn, play_again, timeout, tangcap, end;
var current_level = localStorage.getItem("level") || 1;
var plus;
var matrix_colors = {
    "dacam": "do+vang",
    "xanhlacay": "xanhnuocbien+vang",
    "hong": "do+trang",
    "tim": "do+xanhnuocbien",
}
var level = {
    1: "tuong_do",
    2: "tuong_vang",
    3: "tuong_xanhnuocbien",
    4: "tuong_dacam",
    5: "tuong_hong",
    6: "tuong_tim",
    7: "tuong_xanhlacay",
}
var questions = {
    1: "            Màu đỏ ở đâu bé nhỉ ?",
    2: "            Màu vàng ở đâu bé nhỉ ?",
    3: "Màu xanh nước biển ở đâu bé nhỉ ?",
    4: "Dùng 2 màu gì để tạo ra màu da cam nhỉ ?",
    5: "Dùng 2 màu gì để tạo ra màu hồng nhỉ ?",
    6: "Dùng 2 màu gì để tạo ra màu tím nhỉ ?",
    7: "Dùng 2 màu gì để tạo ra màu xanh lá cây ?",
}


window.onload = function () {
    let gameConfig = {
        width: 800,
        height: 1420,
        scene: playGame,
        backgroundColor: 0x222222
    }
    game = new Phaser.Game(gameConfig);
    window.focus()
    resize();
    window.addEventListener("resize", resize, false);
}
//score += 10;scoreText.setText("Score: " + score);
class playGame extends Phaser.Scene {
    constructor() {
        super("PlayGame");
    }
    preload() {
        // Setup mp3
        let lstMp3 = ["win", "failed", "right", "right1", "right2", "combo", "wrong", "botbien", "click"];
        lstMp3.forEach(e => this.load.audio(e, '../mp3/' + e + '.mp3', {
            instances: 1
        }));
        // Setup Image
        let lstImage = ["huongdan","trogiup", "tangcap", "choilai", "hoanthanh", "bangmau", "but-do", "but-trang", "but-vang", "but-xanhdatroi", "but-xanhnuocbien", "back", "huychuong", "home", "timeout", "anhmau", "anhmau_vang", "anhmau_dacam", "anhmau_do", "anhmau_tim", "anhmau_xanhlacay", "anhmau_xanhdatroi", "anhmau_xanhnuocbien", "anhmau_hong", "sai_vang", "sai_dacam", "sai_do", "sai_tim", "sai_xanhlacay", "sai_xanhdatroi", "sai_hong", "dung_vang", "dung_dacam", "dung_do", "dung_tim", "dung_xanhlacay", "dung_xanhdatroi","dung_xanhnuocbien", "dung_hong"];
        lstImage.forEach(e => this.load.image(e, 'images/' + e + '.png'));
        if (current_level > 7) {
            current_level = 1;
        }
        this.load.image("background", 'images/' + level[current_level] + '.png');
        this.load.image("help", '../images/help.png');
        this.load.image("end", '../images/end.png');
        huychuong_text = this.add.text(590, 60, "01", {
            fontSize: "60px",
            fill: "#F5BC2E",
            fontFamily: "Coiny"
        });
        questionsText = this.add.text(140, 500, questions[current_level], {fontSize: "30px",fill: "#000",fontFamily:"Pony"});
    }
    update() {
        if (tangcap.visible) {
            if (tangcap.y > -60) {
                console.log(tangcap.y--);
                tangcap.y = tangcap.y--;
                if (current_level >= 6) {
                    update_score("tronmau", localStorage.getItem("userId"), current_level - 1, score);
                }
                location.reload();
                tangcap.visible = false;
            }
        }
    }
    create() {
        // Tải nhạc

        timeout = this.sound.add('timeout');
        right = this.sound.add('right');
        win = this.sound.add('win');
        failed = this.sound.add('failed');
        right1 = this.sound.add('right1');
        right2 = this.sound.add('right2');
        combo = this.sound.add('combo');
        wrong = this.sound.add('wrong');
        botbien = this.sound.add('botbien');
        click = this.sound.add('click');

        // Cài đặt background
        let form_background = this.add.image(this.cameras.main.width / 2, this.cameras.main.height / 2, 'background');
        let scaleX = this.cameras.main.width / form_background.width;
        let scaleY = this.cameras.main.height / form_background.height;
        let scale = Math.max(scaleX, scaleY);
        form_background.setScale(scale).setScrollFactor(0);

        // Cài đặt nút home
        let home_btn = this.add.image(70, 70, "home");
        home_btn.displayWidth = 90;
        home_btn.displayHeight = 90;

        // Cài đặt nút tăng cấp
        tangcap = this.add.image(400, 100, "tangcap");
        tangcap.displayWidth = 300;
        tangcap.displayHeight = 120;
        tangcap.visible = false;
        tangcap.setDepth(1);
        // Cài đặt nút back
        let back_btn = this.add.image(180, 70, "back");
        back_btn.displayWidth = 120;
        back_btn.displayHeight = 90;
        // Cài đặt bảng màu
        let bangmau = this.add.image(400, 1300, "bangmau");
        bangmau.displayWidth = 800;
        bangmau.displayHeight = 300;
        // Cài đặt hướng dẫn
        let help = this.add.image(50, 250, "help");
        help.setScale(1.3);
        help.setInteractive({
                useHandCursor: true
            })
            .on("pointerdown", function () {
                help.setScale(frame_huongdan.visible ? 1.3 : 1.5);
                frame_huongdan.visible = frame_huongdan.visible ? false : true;
                trogiup.visible = trogiup.visible ? false : true;
                frame_huongdan.setDepth(frame_huongdan.visible ? 1 : 0);
                trogiup.setDepth(frame_huongdan.visible ? 1 : 0);
            })
            .on('pointerover', function () {
                help.setScale(1.5);
            })
            .on('pointerout', function () {
                help.setScale(1.3);
            });
        // Cài đặt ảnh mẫu
        let anhmau = this.add.image(400, 850, "anhmau");
        anhmau.setScale(0.6);
        var this_game = this;
        anhmau.setInteractive({
            useHandCursor: true
        }).on("pointerdown", function () {
            current_color = current_color.replace("but-", "");
            current_tuong = level[current_level].replace("tuong_", "");
            if (colors_selected.length > 1) {
                if ((matrix_colors[current_tuong] == colors_selected[0] + "+" + colors_selected[1]) || (matrix_colors[current_tuong] == colors_selected[1] + "+" + colors_selected[0])) {
                    anhmau = this_game.add.image(399, 850, "anhmau_" + current_tuong);
                } else {
                    alert("Bé chọn chưa đúng màu rồi");
                }
            } else {
                anhmau = this_game.add.image(399, 850, "anhmau_" + colors_selected[0]);
            }
            anhmau.setScale(0.6);
        });
        // Cài đặt huy chương
        let huychuong_btn = this.add.image(720, 90, "huychuong");
        huychuong_btn.displayWidth = 140;
        huychuong_btn.displayHeight = 150;
        // Cài đặt nút chơi lại
        let play_again = this.add.image(630, 1280, "choilai");
        play_again.setScale(0.7);
        // Cài đặt nút hoàn thành
        let hoanthanh = this.add.image(640, 1350, "hoanthanh");
        hoanthanh.setScale(0.6);
        hoanthanh.setInteractive({
            useHandCursor: true
        }).on("pointerdown", function () {
            current_color = current_color.replace("but-", "");
            current_tuong = level[current_level].replace("tuong_", "");
            let dung_sai = "";
            if (colors_selected.length > 1) {
                console.log(matrix_colors[current_tuong]);
                console.log(colors_selected[0] + "+" + colors_selected[1]);
                if ((matrix_colors[current_tuong] == colors_selected[0] + "+" + colors_selected[1]) ||
                    matrix_colors[current_tuong] == colors_selected[1] + "+" + colors_selected[0]
                ) {
                    combo.play();
                    if (current_level < 7) {
                        score = score + current_level * (new Date().getSeconds());
                        current_level++;
                        localStorage.setItem("level", current_level);
                        if(current_level == 7){
                            end.visible = true;
                            end.setDepth(99999);
                            return;
                        }
                        setTimeout(() => {
                            tangcap.visible = true;
                        }, 2000);
                    }
                    dung_sai = "dung_";
                } else {
                    dung_sai = "sai_";
                    wrong.play();
                }
            } else {
                if (current_tuong == colors_selected[0]) {
                    combo.play();
                    if (current_level < 7) {
                        score = score + current_level * (new Date().getSeconds());
                        current_level++;
                        localStorage.setItem("level", current_level);
                        if(current_level == 7){
                            end.visible = true;
                            end.setDepth(99999);
                            return;
                        }
                        setTimeout(() => {
                            tangcap.visible = true;
                        }, 2000);
                    }
                    dung_sai = "dung_";
                } else {
                    dung_sai = "sai_";
                    wrong.play();
                }
            }
            anhmau = this_game.add.image(380, 840, dung_sai + current_tuong);
            anhmau.setScale(0.6);
        });
        // // Cài đặt bút xanh da trời
        // let but_xanhdatroi = this.add.image(100, 1280, "but-xanhdatroi");
        // but_xanhdatroi.setScale(0.7);
        // Cài đặt bút trắng
        let but_trang = this.add.image(180, 1350, "but-trang");
        but_trang.setScale(0.7);
        // Cài đặt bút đỏ
        let but_do = this.add.image(250, 1260, "but-do");
        but_do.setScale(0.8);
        // // Cài đặt bút xanh lá cây
        // let but_xanhlacay = this.add.image(300, 1250, "but-xanhlacay");
        // but_xanhlacay.setScale(0.6);
        // Cài đặt bút vàng
        let but_vang = this.add.image(320, 1355, "but-vang");
        but_vang.setScale(0.8);
        // Cài đặt bút xanh nước biển
        let but_xanhnuocbien = this.add.image(400, 1270, "but-xanhnuocbien");
        but_xanhnuocbien.setScale(0.8);


        // // Cài đặt bút tím
        // let but_tim = this.add.image(210, 1350, "but-tim");
        // but_tim.setScale(0.6);
        // // Cài đặt bút hồng
        // let but_hong = this.add.image(310, 1350, "but-hong");
        // but_hong.setScale(0.6);
        // // Cài đặt bút da cam
        // let but_dacam = this.add.image(410, 1350, "but-dacam");
        // but_dacam.setScale(0.6);
        // Cài đặt sự kiện click cho từng màu
        let lstBut = [but_do, but_trang, but_vang, but_xanhnuocbien];
        var lstFisrt = [];
        lstBut.forEach(function (item) {
            item.setInteractive({
                useHandCursor: true
            }).on("pointerdown", function () {
                if (frame_huongdan.visible) {
                    frame_huongdan.visible = false;
                    trogiup.visible = false;
                    trogiup.setDepth(0);
                    frame_huongdan.setDepth(0);
                }
                // Đặt opacity cho item đang chọn
                current_color = item.texture.key;
                let selectedColor = current_color.replace("but-", "");
                var current_index = 0;
                colors_selected.push(selectedColor);
                if (item.alpha == 1) {
                    item.setAlpha(0.2);
                    lstFisrt.push(item);
                } else {
                    if (colors_selected.length > 0) {
                        colors_selected.forEach(function (color) {
                            if (color == selectedColor) {
                                colors_selected.remove_by_value(color);
                            }
                        });
                    }
                    item.setAlpha(1);
                    lstFisrt.remove_by_value(item);
                }
                colors_selected = Array.from(new Set(colors_selected));
                if (colors_selected.length > 2) {
                    lstFisrt[current_index].setAlpha(1);
                    lstFisrt.shift();
                    colors_selected.shift();
                }
                // console.log(lstFisrt);
            });
        });


        huychuong_text = this.add.text(590, 60, "01", {
            fontSize: "60px",
            fill: "#F5BC2E",
            fontFamily: "Coiny"
        });

        this.canPick = true;
        this.dragging = false;
        home_btn.setInteractive({
            useHandCursor: true
        }).on('pointerdown', this.home_click);
        form_background.setInteractive({
            useHandCursor: false
        }).on("pointerdown", this.form_load);
        huychuong_btn.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.huychuong_btn_click);
        back_btn.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.back_click);
        play_again.setInteractive({
            useHandCursor: true
        }).on("pointerdown", this.play_again_click);
        frame_huongdan = this.add.image(400, 420, "huongdan");
        trogiup = this.add.image(400, 780, "trogiup");
        frame_huongdan.setScale(0.4);
        trogiup.setScale(0.45);
        frame_huongdan.setInteractive({
            useHandCursor: true
        }).on("pointerdown", function () {
            frame_huongdan.visible = false;
            trogiup.visible = false;
            trogiup.setDepth(0);
            frame_huongdan.setDepth(0);
        });
        if (localStorage.getItem("frame_huongdan") == "true" || localStorage.getItem("frame_huongdan") == null) {
            frame_huongdan.visible = true;

        } else {
            frame_huongdan.visible = false;
        }
        frame_huongdan.visible = false;
        trogiup.visible = false;
        questionsText = this.add.text(140, 500, questions[current_level], {fontSize: "30px",fill: "#000",fontFamily:"Pony"});
        // Cài đặt nút END
        end = this.add.image(450, 600, "end");
        end.setScale(0.4);
        end.setDepth(999999);
        end.visible = false;
        end.setInteractive( { useHandCursor: true  }).on("pointerdown", this.home_click);
    }
    form_load() {
        
        // console.log(localStorage.getItem("frame_huongdan"));
        // if(localStorage.getItem("frame_huongdan")){
        //     frame_huongdan.visible = false;
        //     start_btn.visible = false;
        // }
    }
    huychuong_btn_click() {
        if (current_level < 6) {
            console.log(current_level++);
            localStorage.setItem("level", current_level++);
            location.reload();
        }
    }
    back_click() {
        if (current_level > 1) {
            console.log(current_level--);
            localStorage.setItem("level", current_level--);
            location.reload();
        }
    }
    play_again_click() {
        localStorage.removeItem("begin");
        // localStorage.removeItem("frame_huongdan");
        location.reload();
    }
    start_click() {
        localStorage.setItem("begin", "start");
        frame_huongdan.visible = false;
        localStorage.setItem("frame_huongdan", false);
        document.getElementsByTagName("canvas")[0].classList.remove("not_allowed");
        console.log(lstNangLuong);
    }
    home_click() {
        location.href = "../index.html";
        // frame_huongdan.visible = true;
        // start_btn.visible = true;
        // plus.visible = false;
        // frame_huongdan.setDepth(1);
        // start_btn.setDepth(1);
        // if(start_btn.y >= 1020)
        //     start_btn.y = start_btn.y - 280;
    }

}
Array.prototype.remove_by_value = function (val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === val) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
}

function resize() {
    var canvas = document.querySelector("canvas");
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var windowRatio = windowWidth / windowHeight;
    var gameRatio = game.config.width / game.config.height;
    if (windowRatio < gameRatio) {
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    } else {
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}